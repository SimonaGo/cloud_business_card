const common = require('config.js');
const getQodeScene = require('getQodeScene.js');
// const app = getApp();
var optionsText = '';
var unid = '';
var authorObj = {
  optionsText: '',
  unid: '',
  userinfor:[],
  authorFun: function (app, res, options) {
    var THIS = this;
    for (var i in options) {
      optionsText += '&' + i + '=' + options[i];
    }
    optionsText = optionsText.slice(1);
    THIS.qodePara = getQodeScene.getPara(options);//将头部表示转化为数组
    console.log(options.scene);
    wx.request({//获取凭证
      // url: common.domain + '/getUserInfoStatus.form',
      url: common.formal+ '/member/confine/getUserInfoStatus.form',
      data: {
        test: common.test,
        code: res.code,//要去换取openid的登录凭证,
        vrTag: options.scene
      },
      method: 'get',
      success: function (getUserInfoStatus) {
        console.log(getUserInfoStatus.data)
        console.log(getUserInfoStatus);
        if (getUserInfoStatus.data.unionid != undefined) {
          app.globalData.unionid = getUserInfoStatus.data.unionid;
          console.log(app.globalData.unionid)
          unid = getUserInfoStatus.data.unionid;
          if (app.unionidCallback) {
            app.unionidCallback(getUserInfoStatus.data.unionid);
          }
        }
        if (getUserInfoStatus.data.status == "0") {
          wx.showToast({
            title: '小程序授权出错，请稍后再试',
            icon: 'none',
            duration: 2000
          })
        }
        console.log('stop')
        console.log(getUserInfoStatus)
        var sessionKey = getUserInfoStatus.data.sessionKey;
        var openId = getUserInfoStatus.data.miniOpenid;
        options.openId = getUserInfoStatus.data.miniOpenid;
        if (getUserInfoStatus.data.userInfoStatus == "true") {
          // var options = _this.options;
          console.log(options)
          if (options.scene) {
             if(options.scene.indexOf('cardNo')!=-1){
                console.log('tacard')
               var num = options.scene.replace('cardNo','')
              var redirectToUrl = null;
             }else{
              var redirectToUrl = "/pages/index/index?scene=" + options.scene;
              console.log('index')
             }
          } else if (options.articleId) {
            redirectToUrl = "/pages/" + options.wherePage + "/" + options.wherePage + "?unionid=" + options.unionid + '&vrtag=' + options.vrTag + '&title=' + options.title + '&id=' + options.id + '&flag=' + options.flag + '&articleId=' + options.id 
          }
           else if (options.vrTag) {
             redirectToUrl = "/pages/TaCard/TaCard?objectId=" + options.objectId + '&vrTag=' + options.vrTag + '&imageUrl=' + options.imageUrl + '&memberId=' + options.memberId
          } else if (options.objectId != undefined&&options.objectId.indexOf("base") != -1 ) {
             redirectToUrl = "/pages/TaCard/TaCard?objectId=" + options.objectId + '&vrTag=' + options.vrTag + '&imageUrl=' + options.imageUrl + '&memberId=' + options.memberId
          } else if (options.cardNo) {
            redirectToUrl = "/pages/TaCard/TaCard?cardNo="+options.cardNo;
          }
          else {
            redirectToUrl = "/pages/trans/trans";
          }
          if(redirectToUrl!=null){
            wx.reLaunch({
              url: redirectToUrl
            });
          }
        }
        else {
          wx.getUserInfo({
            withCredentials: true,
            lang: "zh_CN",
            success: userinfo => {
              if (options.scene!=undefined&&options.scene.indexOf('cardNo')!=-1){
               
               var num = options.scene.replace('cardNo','')
               console.log(num)
                var reLaunch = "/pages/TaCard/TaCard?cardNo="+num;
              //  wx.redirectUrl
              wx.reLaunch({
                url: redirectToUrl
              })
             }else{
                var reLaunch = "/pages/index/index?scene=" + options.scene;
              console.log('index')
             }
            },
            fail: function () { 
              console.log('跑了')   //授权失败
              if (options.scene){
                console.log('跑远了')
                wx.reLaunch({
                  url: '/pages/getUserInfo/getUserInfo?scene='+options.scene+'&sessionKey=' + encodeURIComponent(getUserInfoStatus.data.sessionKey) + '&unionid=' + getUserInfoStatus.data.unionid
                });
              } else if (options.vrTag){
                wx.reLaunch({
                  url: '/pages/getUserInfo/getUserInfo?objectId=' + options.objectId + '&sessionKey=' + encodeURIComponent(getUserInfoStatus.data.sessionKey) + '&vrTag=' + options.vrTag + '&imageUrl=' + options.imageUrl + '&memberId=' + options.memberId
                });
              }else{
                wx.reLaunch({
                  url: '/pages/getUserInfo/getUserInfo?sessionKey=' + encodeURIComponent(getUserInfoStatus.data.sessionKey) + '&unionid=' + getUserInfoStatus.data.unionid
                });
              }
            },
          });
        }
      },
      fail:function(){
        wx.showModal({
          title: '请求超时',
          content: '这是一个模态弹窗',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          }
        })
      }
    });
  }
}
exports.authorObj = authorObj;