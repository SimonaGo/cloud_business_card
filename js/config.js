var config={
    //  test:3,//h5页面走6449、3是6444
     test: 2,//h5页面走6449、3是6444
    vrTag:"c0011",   //全景编号
    enter:"vrcard",
    folder:"c",
    shareTitle:"欢迎访问VR云名片",    //分享标题
    domain: "https://minsu.vryundian.com/vrCard", //云店域名
    // formal:"https://minsu.vryundian.com:6444",//测试版
      formal: "https://minsu.vryundian.com:6449",//正式版
    check:"https://minsu.vryundian.com:6449",//正式版
};
module.exports.domain = config.domain;
exports.test = config.test;
exports.vrTag = config.vrTag;
exports.shareTitle = config.shareTitle;
exports.enter = config.enter;
exports.folder = config.folder;
exports.interfacePath = config.interfacePath;
exports.formal = config.formal;
exports.check = config.check;