var getPara = function (options){ //将scene变成数组形式并返回
    var scene = decodeURIComponent(options.scene);
    // var scene = 'userId-123456,vrTag-0000';
    var sceneArr = scene.split(',')
    var para = {};
    for (var i in sceneArr) {
      var name = sceneArr[i].slice(0, sceneArr[i].indexOf('-'));
      var value = sceneArr[i].slice(sceneArr[i].indexOf('-') + 1);
      para[name] = value
    }
    console.log(sceneArr, para);
    return para;
}
exports.getPara = getPara;
