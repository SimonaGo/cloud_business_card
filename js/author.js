const common = require('config.js');
const getQodeScene = require('getQodeScene.js');
var vrTag=null;
var optionsText='';
var unid='';
var authorObj={
   optionsText : '',
   unid : '',
    authorFun: function (obj,res,options){
    var THIS=this;
    for(var i in options){
      optionsText+='&'+i+'='+options[i];
    }
    optionsText = optionsText.slice(1);
    THIS.qodePara = getQodeScene.getPara(options);//将头部表示转化为数组
    if(options.vrTag!=undefined){
      vrTag = options.vrTag;
    }
    else if (THIS.qodePara.tag!=undefined){
      vrTag = THIS.qodePara.tag;
    }
    console.log(vrTag)
      obj.setData({
        vrTag: vrTag
      })
    wx.request({//获取凭证
      url: common.formal + '/member/confine/getUserInfoStatus.form',
      // url: 'https://minsu.vryundian.com:6449/member/confine/getUserInfoStatus.form',
      data: {
        vrTag: vrTag == null ? common.vrTag : vrTag,
        test: common.test,
        code: res.code//要去换取openid的登录凭证
      },
      method: 'get',
      success: function (getUserInfoStatus) {
        // common.test = getUserInfoStatus.data.test;
        console.log(getUserInfoStatus);
        if (getUserInfoStatus.data.unionid!=undefined){
          obj.setData({
            unide: getUserInfoStatus.data.unionid
          })
        }
        if (getUserInfoStatus.data.status=="0"){
            vrTag=common.vrTag;
            wx.showToast({
              title: '小程序授权出错，请稍后再试',
              icon: 'none',
              duration: 2000
            })
        }
        unid = getUserInfoStatus.data.unionid;
        var sessionKey = getUserInfoStatus.data.sessionKey;
        var openId = getUserInfoStatus.data.miniOpenid;
        options.openId = getUserInfoStatus.data.miniOpenid;
        if (getUserInfoStatus.data.userInfoStatus == "true") {
          if(vrTag==null){
            vrTag = common.vrTag;
            wx.reLaunch({
              url: '../index/index?vrTag=' + vrTag
            })
          }
          else {
            if (THIS.qodePara.e == "1") {
              wx.request({
                url: common.formal +'/member/checkMember.form',
                // url: 'https://minsu.vryundian.com:6449/member/checkMember.form',
                data: {
                  test: common.test,
                  vrTag: THIS.qodePara.tag,
                  unionid: unid
                },
                method: 'get',
                success: function (json) {
                  console.log(json)
                  if (json.data.status == "1") {
                    wx.reLaunch({
                      url: '../bindPhone/bindPhone?vrTag=' + THIS.qodePara.tag + "&unionid=" + unid
                    })
                  } 
                }
              })
            }

            var unionid = getUserInfoStatus.data.unionid;
            options.unionid = getUserInfoStatus.data.unionid;
            if (options.toUrl != undefined) {
              obj.toViewUrl();
              console.log(options.toUrl)
            }
            else {
              var para = THIS.getPara(options, common);
              obj.setData({
                vrurl: 'https://oss.wdsvip.com/' + common.folder + '/' + vrTag + '/index.html?userInfoStatus=true' + para
              });
              
            }
          }
        }
        else {
          wx.getUserInfo({
            withCredentials: true,
            lang: "zh_CN",
            success: userinfo => { 
              //授权成功
              wx.request({
                url: common.formal + "/member/confine/saveUserInfo.form",
                // url: 'https://minsu.vryundian.com:6449/member/confine/saveUserInfo.form',
                data: {
                  test: common.test,
                  vrTag: vrTag == null ? common.vrTag : vrTag,
                  nickName: userinfo.userInfo.nickName,
                  avatarUrl: userinfo.userInfo.avatarUrl,
                  gender: userinfo.userInfo.gender,
                  city: userinfo.userInfo.city,
                  province: userinfo.userInfo.province,
                  country: userinfo.userInfo.country,
                  language: userinfo.userInfo.language,
                  iv: userinfo.iv,
                  encryptedData: userinfo.encryptedData,
                  discountTypes: "0",
                  sessionKey: getUserInfoStatus.data.sessionKey,
                  unionid: getUserInfoStatus.data.unionid
                },
                success: function (saveUserInfo) { 
                 unid = saveUserInfo.data.unionid;
                  console.log(saveUserInfo)
                  obj.setData({
                    unide: unid
                  }) 
                   if (THIS.qodePara.e == "1") {
                    wx.request({
                      url: common.formal +'/member/checkMember.form',
                      data:{
                        test: common.test,
                        vrTag: THIS.qodePara.tag,
                        unionid: unid
                      },
                      method: 'get',
                      success: function (json) {
                          if(json.data.status =="1"){
                            wx.reLaunch({
                              url: '../bindPhone/bindPhone?vrTag=' + THIS.qodePara.tag + "&unionid=" + unid
                            })
                          }
                      }
                    })
                    THIS.qodePara['unionid'] = saveUserInfo.data.unionid;
                    THIS.qodePara['test'] = common.test;
                  }
                  options.unionid = saveUserInfo.data.unionid;
                  if (options.toUrl != undefined) {
                    obj.toViewUrl(options);
                    console.log(options.toUrl)
                  }
                  else {
                    vrTag = vrTag==null?common.vrTag:vrTag;
                    var para = THIS.getPara(options, common);
                    obj.setData({
                      vrurl: 'https://oss.wdsvip.com/' + common.folder + '/' +vrTag + '/index.html?userInfoStatus=true'+para
                    });
                  }

                }
              })
            },
            fail: function () {    //授权失败
              if (options.toUrl != undefined) {
                obj.toViewUrl(options);
                console.log(options.toUrl)
              }
              else {
                wx.navigateTo({
                  url: '/pages/getUserInfo/getUserInfo?' + optionsText
                });
                var para = THIS.getPara(options, common);
                vrTag = vrTag == null ? common.vrTag : vrTag;
                obj.setData({
                  vrurl: 'https://oss.wdsvip.com/' + common.folder + '/' + vrTag + '/index.html?&userInfoStatus=false' + para
                });
              }
            },
          });
        }
      },
    });
  },
  getPara: function (options, common){
    var para = '&miniOpenid=' + options.openId + '&unionid=' + options.unionid + '&vrTag=' + vrTag + '&enter=' + common.enter + '&test=' + common.test + '&proTag=miniwx' +'&objectId='+options.objectId;
    return para;
  }
}
var authorFun = function (res,options){
}
exports.authorObj = authorObj;