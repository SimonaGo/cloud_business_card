//app.js
const author = require('js/appInit.js');
const common = require('js/config.js');
App({
  slideupshow: function (that, param, px, opacity) {
    var animation = wx.createAnimation({
      duration: 800,
      timingFunction: 'ease',
    });
    animation.translateY(px).opacity(opacity).step()
    //将param转换为key
    var json = '{"' + param + '":""}'
    json = JSON.parse(json);
    json[param] = animation.export()
    //设置动画
    that.setData(json)
  },
  //检查微信小程序是否最新版本
checkForUpdate: function () {
  const updateManager = wx.getUpdateManager()
  updateManager.onUpdateReady(function () {
  wx.showModal({
  title: '更新提示',
  content: '新版本已经准备好，是否重启应用？',
  success: function (res) {
  if (res.confirm) {
  updateManager.applyUpdate()
}}})})
  updateManager.onUpdateFailed(function () { })
  },
  onLaunch: function (options) {
    let that = this;
    wx.request({
      url: common.formal + '/getStatus.form',
      data: '',
      method: 'get',
      success: function (json) {
        console.log(json.data.status)
        if (json.data.status == '1') {
          wx.reLaunch({
            url: '../../pages/index/index?unionid=' + 'onvlS5tZ4uqFzyi9QEGl9NtIDx24' + "&vrTag=" + 'c0001' + "&meobjectId=" + '8TCKD1K3G5LZ1OB15FDV' + "&imageUrl=" + 'http://oss.wdsvip.com/image/vrCard/share/enterprise/wds1567041564847.png' + "&identityStatus=" + '2' + "&objectType=" + '2' + "&objectId=" + '8TCKD1K3G5LZ1OB15FDV'
          })
        }else{
    that.checkForUpdate()
    // 获取系统信息
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    console.log(options)
    console.log(that.globalData.unionid);
    // 登录
    wx.login({
      success: function (res) {
        console.log(res)
         author.authorObj.authorFun(that, res, options.query)
        },
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
      })
      }
      }
    })
  },
  globalData: {
    userInfo: null,
    vrTag:'',
    unionid:'',
    objectId:'',
    imageUrl:'',
    height:0,
    longitude:'',
    latitude:''

  }
})