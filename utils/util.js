const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  // const hour = date.getHours()
  // const minute = date.getMinutes()
  // const second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('-') 
 
}


function formatTimeTwo(number) {
  var date = new Date(number);
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? ('0' + m) : m;
  var d = date.getDate();
  d = d < 10 ? ('0' + d) : d;
  var h = date.getHours();
  h = h < 10 ? ('0' + h) : h;
  var minute = date.getMinutes();
  var second = date.getSeconds();
  minute = minute < 10 ? ('0' + minute) : minute;
  second = second < 10 ? ('0' + second) : second;
  // return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;//年月日时分秒
  return y + '-' + m + '-' + d ;
}


const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}
//todate默认参数是当前日期，可以传入对应时间 todate格式为2018-10-05

function getDates(days, todate) {
  var dateArry = [];
  for (var i = 0; i < days; i++) {
    var dateObj = dateLater(todate, i);
    dateArry.push(dateObj)
  }
  return dateArry;
}
function dateLater(dates, later) {
  let dateObj = [];
  let show_day = new Array('周日', '周一', '周二', '周三', '周四', '周五', '周六');
  let show_month = new Array('一月', '二月', '三月', '四月', '五月', '陆月', '七月', '八月', '九月', '十月', '十一月', '十二月');
  let date = new Date(dates);
  date.setDate(date.getDate() + later);
  let day = date.getDay();
  let yearDate = date.getFullYear();
  let month = ((date.getMonth() + 1) < 10 ? ((date.getMonth() + 1)) : date.getMonth() + 1);
  let dayFormate = (date.getDate() < 10 ? ("0" + date.getDate()) : date.getDate());
  dateObj.time =  yearDate+'-'+ month + '-' + dayFormate;
  dateObj.week = show_day[day];
  dateObj.month =show_month[month-1]
  return dateObj;
}
function splitarr(str){
  return str.split('-')
}
module.exports = {
  formatTime: formatTime,
  getDates:getDates,
  splitarr:splitarr,
  formatTimeTwo: formatTimeTwo
}


