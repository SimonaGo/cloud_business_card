// pages/newsDynamicDetail/newsDynamicDetail.js
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
var WxParse = require('../../dist/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrollTop: 0,
    share:false
  },
  toTaCard:function(){
    if (this.data.objectId != null && this.data.objectId != undefined && this.data.vrtags != null && this.data.vrtags != undefined)
      wx.navigateTo({
        url: "./../TaCard/TaCard?objectId=" + this.data.objectId + '&vrTag=' + this.data.vrtags + '&imageUrl=' + this.data.imageUrl,
      })
  },
  taCardInit: function (unionid) {
    var _that = this
    if(unionid){
      _that.setData({
        share:true,
      })
      wx.request({
        url: common.formal + '/transInit.form',
        data: {
          unionid: unionid,
          contactList: 'false',
          start: 0,
          end: 10
        },
        method: 'get',
        success: function (json) {
          var someone = json.data.data
          _that.setData({
            someone: someone,
            objectId: someone.objectId,
            vrtags: someone.vrTag,
            imageUrl: someone.imageUrl
          })
         }
      })
    }else{
        console.log('没有uniond')
    }
   
  },
  toaddition:function(){
    wx.redirectTo({
      url: '../base_card_addition/base_card_addition'
    })
  },
  tonewsDetail: function (e) {
    wx.navigateTo({
      url: './../newsDynamicDetail/newsDynamicDetail?id=' + e.currentTarget.dataset.id + '&title=' + e.currentTarget.dataset.title + '&vrTag=' + e.currentTarget.dataset.vrtag + '&flag=' + e.currentTarget.dataset.flag,
    })
  },
  elsenewsInit: function (flag) {
    var _that = this
    wx.request({
      url: common.formal + '/articleZW/selectAllArticleZW.form',
      data: {
        articleType: flag,
        start: 0,
        end: 5
      },
      method: 'get',
      success: function (json) {
        let elsenewsArray = json.data.data;
        if (elsenewsArray.length != '0' && elsenewsArray != '' && elsenewsArray!= 'null') {
          _that.setData({
            elsenewsArray: elsenewsArray
          })
        } else {
          _that.setData({
            elsenewsArray: elsenewsArray
          })
        }
      }
    })
  },
  newsInit:function(id){
    var that = this
    wx.request({
      url: common.formal + '/articleZW/selectArticleZW.form',
      data: {
        articleId:id
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        wx.hideLoading()
        that.setData({
          newsInitDetail: json.data.data,
          giveLike: json.data.data.giveLikeNum
        })
        var content = json.data.data.content
        if (content !=null){
          WxParse.wxParse('content', 'html', content, that, 5)
        }
      }
    })
  },
  Goback: function () {
    wx.navigateBack({
      delta: 1
    })
  },
  enterpriseMsg: function (vrTag) {
    let _that = this
    wx.request({
      url: common.formal + '/enterprise/getEnterpriseMsg.form',
      data: {
        vrTag: vrTag
      },
      method: 'get',
      success: function (json) {
        _that.setData({
          enterpriseDetail: json.data.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    let _that = this
    _that.setData({
        title: options.title,
        flag: options.flag,
        id: options.id,
        vrTag: options.vrTag,
        intro: options.intro,
        
      })
    if (options.imag != undefined && options.imag != null && options.imag != ''){
      _that.setData({
        imag: options.imag
      })
    }
    _that.taCardInit(options.unionid)
    _that.newsInit(options.id)
    _that.enterpriseMsg(options.vrTag)
    _that.elsenewsInit(options.flag)
    wx.getSystemInfo({
      success: function (res) {
        // 获取可使用窗口宽度
        let clientHeight = res.windowHeight;
        // 获取可使用窗口高度
        let clientWidth = res.windowWidth;
        // 算出高度(单位rpx)
        let height = clientHeight
        // 设置高度
        _that.setData({
          height: height
        })
      },
    });
    if (app.globalData.unionid && app.globalData.unionid != '') {
      let unionid = app.globalData.unionid;
      _that.setData({
        unionid: unionid
      })
    } else {
      app.unionidCallback = unionid => {
        if (unionid != '') {
          _that.setData({
            unionid: unionid
          })
        }
      }
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
   
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var _this = this;
    console.log(res.target, res.target.dataset.pare, res.target.dataset.imag, _this.data.id)
    if (res.from === 'button') {
    }
    return {
      title: _this.data.intro,
      path: "pages/newsDynamicDetail/newsDynamicDetail?unionid=" + _this.data.unionid + '&vrTag=' + _this.data.vrTag + '&title=' + _this.data.title + '&id=' + _this.data.id + '&flag=' + _this.data.flag + '&articleId=' + _this.data.id + '&wherePage=' + 'newsDynamicDetail' + '&imag=' + res.target.dataset.imag == null || res.target.dataset.imag == undefined || res.target.dataset.imag == '' ? _this.data.imag : res.target.dataset.imag,
      imageUrl: res.target.dataset.imag == null || res.target.dataset.imag == undefined || res.target.dataset.imag == '' ? _this.data.imag : res.target.dataset.imag
    }
  }
})