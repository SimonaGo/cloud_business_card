// pages/enterpriseCard/enterpriseCard.js
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
var WxParse = require('../../dist/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    companyPicture: '../../img/companyIntroduction.png',
    showpositionList:true,
    offNews:false,
    offProduct:false,
    freeForCompany:true
  },
  // onPageScroll: function (e) {
  //   console.log(e.scrollTop)
  // },
  toVr: function () {
    console.log(this.data.unionid, this.data.vrTag, this.data.objectId, this.data.imageUrl, this.data.identityStatus, this.data.objectType)
   
    wx.navigateTo({
      url: '../index/index?unionid=' + this.data.unionid + "&vrTag=" + this.data.vrTag + "&meobjectId=" + this.data.objectId + "&imageUrl=" + this.data.imageUrl + "&identityStatus=" + this.data.identityStatus + "&objectType=" + this.data.objectType + "&objectId=" + this.data.objectId
    })
  },
  gotrans:function(){
    wx.switchTab({
      url: './../trans/trans',
    })
  },
  inforInit: function (vrTag){
    var _that = this
    wx.request({
      url: common.formal + '/enterprise/getEnterpriseMsg.form',
      data: {
        vrTag: vrTag,
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        var companyList=json.data.data
        if (companyList.length != '0' && companyList != '' && companyList != 'null') {
          _that.setData({
            companyList: json.data.data,
            showcompanyList: true,
            alldata: json.data.data.length,
            mobilePhone: json.data.data.enterprisePhone,
            latitude: json.data.data.latitude,
            longitude: json.data.data.longitude,
            enterpriseName: json.data.data.enterpriseName,
            vrTag: json.data.data.vrTag
          })
        } else {
          _that.setData({
            companyList: json.data.data,
            showcompanyList: false,
            vrTag: json.data.data.vrTag
          })
        }

        if (json.data.data.identityStatus == '1' ) {
          // 企业基础版
          _that.setData({
            freeForCompany: true,
          })
        } else if (json.data.data.identityStatus == '2') {
          // 企业升级版
          _that.setData({
            freeForCompany: false,
         
          })
        }
  }})},
  gotoMap: function () {
        var _that = this
        let plugin = requirePlugin('routePlan');
        let key = 'EWDBZ-LNTW6-OBISF-M7XFS-RJVTF-ELB3L';  //使用在腾讯位置服务申请的key
        let referer = 'vr云名片';   //调用插件的app的名称
        let endPoint = JSON.stringify({  //终点
          'name': _that.data.enterpriseName,
          'latitude': _that.data.latitude,
          'longitude': _that.data.longitude
        });
        wx.navigateTo({
          url: 'plugin://routePlan/index?key=' + key + '&referer=' + referer + '&endPoint=' + endPoint
        });
  },
  callCusPhone: function () {
    wx.makePhoneCall({
      phoneNumber: this.data.mobilePhone,
      success: function () {
        console.log("拨打电话成功！")
      },
      fail: function () {
        console.log("拨打电话失败！")
      }
    })
  },
  topositionDetail:function(e){
    wx.navigateTo({
      url: './../positionDetail/positionDetail?id=' + e.currentTarget.dataset.id + '&vrtag=c0011',
    })
  },
  positionInit: function (vrTag) {
    var _that = this
    wx.request({
      url: common.formal + '/vrCard/getPositionList.form',
      data: {
        vrTag: vrTag,
        articleStatus: "1",
        articleType: '2'
      },
      method: 'get',
      success: function (json) {
        console.log(json.data.data)
        var positionList = json.data.data
        if (positionList.length != '0' && positionList != '' && positionList != 'null') {
          _that.setData({
            positionList: json.data.data,
            showpositionList: true,
            alldata: json.data.data.length
          })
        } else {
          _that.setData({
            positionList: json.data.data,
            showpositionList: false
          })
        }
      }
    })
  },
  newsInit: function (vrTag) {
    var _that = this
    wx.request({
      url: common.formal + '/getProductList.form',
      data: {
        vrTag: vrTag,
        articleStatus: "1",
        articleType: '2'
      },
      method: 'get',
      success: function (json) {
        console.log(json.data.data)
        let newsArray = json.data.data;
        console.log(newsArray.length)
        if (newsArray == '' || newsArray == null || newsArray == undefined || newsArray.length=='0'){
          _that.setData({
            newsArray: newsArray,
            offNews:false
          })
          console.log(_that.data.offNews)
        }else{
          _that.setData({
            newsArray: newsArray,
            offNews: true
          })
          console.log(_that.data.offNews)
          var content = json.data.data.content
          WxParse.wxParse('content', 'html', content, _that, 5)
        }
      }
    })
  },
  productInit: function (vrTag) {
    var _that = this
    wx.request({
      url: common.formal + '/getProductList.form',
      data: {
        vrTag: vrTag,
        articleStatus: "1",
        articleType: '1'
      },
      method: 'get',
      success: function (json) {
        console.log(json.data.data)
        let productArray = json.data.data;
        if (productArray == null || productArray == '' || productArray==undefined){
          _that.setData({
            productArray: productArray,
            offProduct:false
          })
        }else{
          _that.setData({
            productArray: productArray,
            offProduct: true
          })
          var content = json.data.data.content
          WxParse.wxParse('content', 'html', content, _that, 5)
        }
      }
    })
  },
  tonewsDetail:function(e){
    var that = this
    wx.navigateTo({
      url: './../newsDynamicDetail/newsDynamicDetail?id=' + e.currentTarget.dataset.id + '&companyName=' + that.data.enterpriseName + '&vrTag=' + that.data.vrTag + '&flag=' + e.currentTarget.dataset.flag + '&title=' + e.currentTarget.dataset.title
    })
  },
  taCardInit: function (unionid) {
    var _that = this
    wx.request({
      url: common.formal + '/transInit.form',
      data: {
        unionid: unionid,
        contactList: true,
        start: 0,
        end: 5
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        _that.setData({
          objectId:json.data.data.objectId,
          meobjectId: json.data.data.objectId,
          imageUrl: json.data.data.imageUrl,
          identityStatus: json.data.data.identityStatus,
          objectType: json.data.data.objectType
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _that = this
    console.log(options)
    var that = this
    this.positionInit(options.vrtag)
    this.inforInit(options.vrtag)
    this.newsInit(options.vrtag)
    this.productInit(options.vrtag)
    if (app.globalData.unionid && app.globalData.unionid != '') {
      let unionid = app.globalData.unionid;
      _that.setData({
        unionid: unionid
      })
      _that.taCardInit(unionid)
    } else {
      app.unionidCallback = unionid => {
        if (unionid != '') {
          _that.setData({
            unionid: unionid
          })
          _that.taCardInit(unionid)
        }
      }
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})