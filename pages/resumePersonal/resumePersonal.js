//index.js
//获取应用实例
const util = require('../../utils/util');
const area = require("../../utils/area.js");

var app = getApp()
Page({
  data: {
    condition: false,
    condition1: false,
    condition2: false,
    condition5:false,
    currentDate: '',
    currentPrice: '',
    expectedSalary: '',
    minDate: new Date().getTime(),
    maxDate: new Date().setDate(new Date().getDate() + 7),
    columns1: ['5k以下', '6k', '7k', '8k', '9k', '10k', '11k', '12k', '13k', '13k以上'],
    columns2: ['5k以下', '6k', '7k', '8k', '9k', '10k', '11k', '12k', '13k', '13k以上'],
    areaList: ''
     
  },
  open1(){
    this.setData({
      condition5: !this.data.condition5,
      condition1: false,
      condition2: false,
    })
  },
  onInput(event) {
    console.log(event.detail)
    this.setData({
      currentDate: util.formatTimeTwo(event.detail)
    });
  },
  onConfirm(event) {
    const { picker, value, index } = event.detail;
    Toast(`当前值：${value}, 当前索引：${index}`);
  },
  onConfirm5(event) {
    this.setData({
      condition5:false,
      condition1: false,
      condition2: false,
    })
  },
  oncancel5(event) {
    this.setData({
      condition5: false,
      condition1: false,
      condition2: false,
    })
  },
  onChange1(event) {
    const { picker, value, index } = event.detail;
    this.setData({
      currentPrice: `${value}`,
    })
  },
  onChange2(event) {
    const { picker, value, index } = event.detail;
    this.setData({
      expectedSalary: `${value}`
    })
  },
  onChange5(event) {
    const { picker, value, index } = event.detail;
    console.log(event.detail.values)
    
    this.setData({
      address: event.detail.values[0].name+'-',
      address1: event.detail.values[1].name + '-' == '' || event.detail.values[1].name == undefined ? '' : event.detail.values[1].name+'-',
      address2: event.detail.values[2].name == '' || event.detail.values[2].name == undefined ? '' : event.detail.values[2].name,
    })
  },
  oncancel() {
    this.setData({
      condition: !this.data.condition
    })
  },
  open: function () {
    this.setData({
      condition: !this.data.condition,
      condition1: false,
      condition2: false,
    })

  },
  open_van1: function () {
    this.setData({
      condition1: !this.data.condition1,
      condition: false,
      condition2: false
    })
  },
  open_van2: function () {
    this.setData({
      condition2: !this.data.condition2,
      condition: false,
      condition1: false
    })
  },
  onLoad: function () {
    area.init(this)
  }
})
