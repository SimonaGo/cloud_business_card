//index.js
//获取应用实例
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
let touchDotX = 0;//X按下时坐标
let touchDotY = 0;//y按下时坐标
let interval;//计时器
let time = 0;//从按下到松开共多少时间*100
Page({
  data: {
    friendLists:false,
    showOrmiss:false,
    userid:'',
    vrTag:'',
    imageUrl:'',
    objectId:'',
    times:0,
    hideLoadMore:true,
    showloadmore:true,
    allfriend:'全部',
    condition:0,
    alldata:0,
    memberId:'',
    unionid:'',
    companyInfor: '',
    binds:0,
    height:0,
    h1:'',
    erWeiMa2:'none',
    here:'查看全部',
    imagemore:"../../img/gengduo.png",
    friendList:[],
    src: '../../img/ps.png',
    img_url: '',
    noContactlist:false,
    visitor:false,
    img_url: '',
    indexOfnumbers:'1',
    index:'1',
    currentTab: 0, //预设当前项的值
    scrollLeft: 0, //切换栏的滚动条位置
    touchDotX : 0,//X按下时坐标
     touchDotY : 0,//y按下时坐标
    interval,//计时器
    ani:'',
    displays:'none',
    time : 0,//从按下到松开共多少时间*100
    img_background: 'http://a.wdsvip.com/vrCard/enterprise/wds.png',
    'censusList': [
      {
        type: "访客",
        number: "0",
        img_url:'../../img/yanjing1.png'
      },
      {
        type: "标星",
        number: 0,
        event:'addCardToBag',
        img_url:"../../img/xingx1.png"
      },
      {
        type: "点赞",
        number: "0",
        event: 'giveLikeToCard',
        img_url:"../../img/dianzan1.png"
      }
    ],
  },
   //事件处理函数
  showInput: function () {
    this.setData({
        inputShowed: true
    });
},
tolower:function(){
  this.setData({
    displays:'block'
  })
},
allTaCard: function () {
  if(this.data.showOrmiss){
    this.setData({
      condition:3,
      showOrmiss:!(this.data.showOrmiss),
      here:"查看全部"
    });
  }else{
    this.setData({
      condition: this.data.alldata,
      showOrmiss:!(this.data.showOrmiss),
      here:"收起全部"
     
    });
  }
},
hideInput: function () {
    this.setData({
        inputVal: "",
        inputShowed: false
    });
},
clearInput: function () {
    this.setData({
        inputVal: ""
    });
},
inputTyping: function (e) {
    this.setData({
        inputVal: e.detail.value
    });
},
  selectAll:function(){
    if(this.data.condition>=this.data.alldata){
      if (this.data.alldata==0){
        wx.showToast({
          title: '暂无联系人',
        })
      }else{
        wx.showToast({
          title: '已加载全部',
        })
      }
    }else{
      this.setData({
        condition: this.data.alldata,
        showloadmore: false
      });
    }
  }, 
  selecthree: function () {
    this.setData({
      condition: 3,
      showloadmore: true
    });
  },
  moreMyCard: function () {
    wx.navigateTo({
      url: '../allmyCard/allmyCard'
    })
  },
  toTaCard: function (e) {
    var that = e.currentTarget.dataset
    wx.navigateTo({
      url: '../TaCard/TaCard?objectId=' + that.objectid + '&vrTag=' + that.vrtag + '&imageUrl=' + that.imageurl + '&memberId=' + this.data.memberId
    })
  },
  toSomeOne: function (e) {
    var that = e.currentTarget.dataset
    wx.navigateTo({
      url: '../carDetail/carDetail?objectId=' + that.objectid + '&vrTag=' + that.vrtag + '&imageUrl=' + that.imageurl + '&memberId=' + this.data.memberId + '&unionid=' + this.data.unionid+'&identityStatus='+this.data.identityStatus+'&objectType='+this.data.objectType
    })
  },
  toaddition: function () {
    wx.reLaunch({
      url: '/pages/base_card_addition/base_card_addition'
    })
  },
  selectAvatarUrl: function (e) {
    var _this =this
    wx.request({
      url: common.formal + '/member/selectAvatarUrl.form',
      data: {unionid:e},
      method: 'get',
      success :function (json){
        _this.setData({
          avatarUrl:json.data.data
        })
      }

    })
  },
  giveLikeToCard: function () {
    var _that = this;
    wx.request({
      // url: common.domain + '/mini/giveLikeToCard.form',
      url: common.formal + '/card/giveLikeToCard.form',
      // url: 'https://minsu.vryundian.com:6449/card/giveLikeToCard.form',
      data: {
        objectId: _that.data.objectId,
        unionid: _that.data.unionid,
        test: common.test
      },
      method: 'get',
      success: function (json) {
        if (json.data.status == '1') {
          _that.setData({
            'censusList[2].number': parseInt(_that.data.censusList[2].number) + 1
          })
        } else if (json.data.status == '2'){
          wx.showToast({
            title: '已点过赞',
          })
        }
      }
    })
  },
  addCardToBag: function (objectId, memberId, test) {
    var _that = this;
    wx.request({
      // url: common.domain + '/mini/addCardToBag.form',
      url: common.formal + '/card/addCardToBag.form',
      // url: 'https://minsu.vryundian.com:6449/card/addCardToBag.form',
      data: {
        objectId: _that.data.objectId,
        unionid: _that.data.unionid,
        test: common.test
      },
      method: 'get',
      success: function (json) {
        if (json.data.status == '1') {
          _that.setData({
            'censusList[1].number': parseInt(_that.data.censusList[1].number) + 1
          })
        } else if (json.data.status == '2') {
          wx.showToast({
            title: '您已收藏过',
          })
        }
      }
    })
  },
  // toVr: function () {
  //   console.log(this.data.unionid,this.data.vrTag,this.data.objectId,this.data.imageUrl,this.data.identityStatus,this.data.objectType)
  //   wx.navigateTo({
  //     url: '../index/index' + "&vrTag=" + this.data.vrTag + "&identityStatus=" + this.data.identityStatus 
  //   })
  // },
  taCardInit: function (unionid) {
    var _that = this
    wx.request({
      url: common.formal + '/transInit.form',
      data: {
        unionid: unionid,
        contactList: true, 
        start:0,
        end:5
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        // 类型判断开始
        _that.setData({
          identityStatus1: json.data.data.identityStatus,
          vrTag1: json.data.data.vrTag,
          meobjectId: json.data.data.objectId,
          objectId: json.data.data.objectId
        })
        if(json.data.data.identityStatus=='1'){
          // 企业基础版
          _that.setData({
            freeForCompany:true,
            colleageList:true,
            onlyPersonal:false
          })
      }else if(json.data.data.identityStatus=='2'){
        // 企业升级版
          _that.setData({
            updateForCompany:true,
            colleageList:true,
            onlyPersonal:false
          })
      }else if(json.data.data.identityStatus=='1'&&json.data.data.objectType=='2'){
            // 个人基础版
            _that.setData({
              colleageList:false,
              onlyPersonal:true,
              personal:true
            })
      }else if(json.data.data.identityStatus=='2'&&json.data.data.objectType=='2'){
        // 个人升级版
        _that.setData({
          colleageList:false,
          onlyPersonal:true,
          personal: true
        })
      }
      // 类型判断结束
        if (json.data.status == '3' || json.data.status == '5' || json.data.status == '6'){
            wx.redirectTo({
              url:'../base_card_addition/base_card_addition'
            })
          _that.setData({
            visitor:true
          })
        }
       else{
         if(json.data.status!=0){
              app.globalData.imageUrl = json.data.data.imageUrl,
              app.globalData.objectId = json.data.data.objectId,
              app.globalData.vrtag = json.data.data.vrTag
            if (json.data.data.contactList.length =='0'||json.data.data.contactList==null) {
              _that.setData({
                friendLists:true,
                "companyInfor[0]": json.data.data,
                vrTag: json.data.data.vrTag,
                objectId: json.data.data.objectId,
                imageUrl: json.data.data.imageUrl,
                hideLoadMore: false,
                showloadmore: true,
                alldata: 0,
                memberId: json.data.data.memberId,
                noContactlist:true,
                identityStatus:json.data.data.identityStatus,
                objectType:json.data.data.objectType,
                objectId:json.data.data.objectId,
                unionid:json.data.data.unionid
              })
            } else if (json.data.data.vrTag == null){
              _that.setData({
                "companyInfor[0]": json.data.data,
                friendList: json.data.data.contactList,
                vrTag: json.data.data.vrTag,
                objectId: json.data.data.objectId,
                imageUrl: json.data.data.imageUrl,
                hideLoadMore: false,
                showloadmore: true,
                alldata: json.data.data.contactList.length,
                memberId: json.data.data.memberId,
                noContactlist:false,
                identityStatus:json.data.data.identityStatus,
                objectType:json.data.data.objectType,
                objectId:json.data.data.objectId,
                unionid:json.data.data.unionid
              })
            }else{
              let arry=new Set();
              arry.add( _that.data.companyInfor[0])
              _that.setData({
                "companyInfor[0]": json.data.data,
                friendList:json.data.data.contactList,
                vrTag: json.data.data.vrTag,
                objectId: json.data.data.objectId,
                imageUrl: json.data.data.imageUrl,
                hideLoadMore: false,
                showloadmore: true,
                alldata: json.data.data.contactList.length,
                memberId: json.data.data.memberId,
                identityStatus:json.data.data.identityStatus,
                objectType:json.data.data.objectType,
                objectId:json.data.data.objectId,
                unionid:json.data.data.unionid
              })
            }
            _that.getinfor(json.data.data.objectId,json.data.data.objectType)
         }

        } 
        _that.setData({
          ready:true
        })
        
      },
      fail: function () {
        wx.hideLoading()
        wx.showModal({
          title: '请求超时',
          content: '请重启小程序或联系数据库管理员',
          success: function (res) {
            if (res.confirm) {
              wx.navigateBack({
                delta: -1
              })
            } else if (res.cancel) {
              wx.navigateBack({
                delta: -1
              })
            } else {
              wx.navigateBack({
                delta: -1
              })
            }
          }
        })
      }
    })
  },
  swiperChange:function(e){
    var that = this;
    that.setData({
      index: e.detail.current + 1
    })
  },
   // 点击切换当前页时改变样式
   swichNav: function (e) {
    var that = this,
        cur = e.target.dataset.current,
        src = e.target.dataset.src;
      
    if (that.data.currentTaB == cur) {
      return false;
    } else {
      that.setData({
        currentTab: cur
      })
    }
    that.setData({
      cardImgSrc: src
    })
  },
  // touchStart: function(e) {
  //   touchDotX = e.touches[0].pageX; // 获取触摸时的原点
  //   touchDotY = e.touches[0].pageY;
  //   // 使用js计时器记录时间    
  //   interval = setInterval(function() {
  //     time++;
  //   }, 100);
  // },
  // touchEnd: function(e) {
  //   console.log(e.target.dataset.current)
  //   var that = this 
  //  that.setData({
  //   index:e.target.dataset.current+1
  //  })
  //   let touchMoveX = e.changedTouches[0].pageX;
  //   let touchMoveY = e.changedTouches[0].pageY;
  //   let tmX = touchMoveX - touchDotX;
  //   let tmY = touchMoveY - touchDotY;
    // if (time < 200) {
    //   let absX = Math.abs(tmX);
    //   let absY = Math.abs(tmY);
    //   if (absX > 2 * absY) {
    //     if (absX < 331){
    //       console.log("左滑=====", absX+'px' )
    //     }else{
    //       console.log("右滑=====", absX+'px')
    //     }
    //   }
    //   if (absY > absX * 2 && tmY<0) {
    //     console.log("上滑动=====")
    //   }
    // }
  //   clearInterval(interval); // 清除setInterval
  //   time = 0;
  // },
  Ercode:function(){
    this.setData({
      erWeiMa2:'block'
    })
},
closeModal:function(){
  this.setData({
    erWeiMa2:'none'
  })
},
toSomeOne: function (e) {
  wx.navigateTo({
    url: '../carDetail/carDetail?objectId=' +this.data.objectId + '&vrTag=' + this.data.vrTag + '&imageUrl=' + this.data.imageUrl + '&memberId=' + this.data.memberId + '&unionid=' + this.data.unionid + '&identityStatus=' + this.data.identityStatus + '&objectType=' + this.data.objectType
  })
},
  getinfor: function (objectId,objectType) {
    var _that = this
    wx.request({
      url: common.formal + '/objectTransInit.form',
      data: {
        objectId: objectId,
        colleagueList:"true",
      },
      method: 'get',
      success: function (json) {
       
        if(objectType=='1'){
          _that.setData({
            companyOrpersonal:false,
            vrTag: json.data.data.vrTag,
            objectInfo: json.data.data.objectInfo,
            enterpriseName: json.data.data.enterpriseName
          })
        }
        if (json.data.data.visitedNum==null){
          json.data.data.visitedNum=0
        } if (json.data.data.collectNum == null) {
          json.data.data.collectNum = 0
        } if (json.data.data.givelikeNum== null) {
          json.data.data.givelikeNum = 0
        }
        // if (json.data.data.phone == null) {
        //   wx.redirectTo({
        //     url: '/pages/msg/msg',
        //   })
        // }
          _that.setData({
            "companyInfor[0]": json.data.data,
            friendList: json.data.data.colleagueList,
            vrTag: json.data.data.vrTag,
            objectId: json.data.data.objectId,
            imageUrl: json.data.data.imageUrl,
            hideLoadMore: false,
            showloadmore: true,
            alldata: json.data.data.colleagueList== null ? '0' : json.data.data.colleagueList.length,
            company: json.data.data.enterpriseName,
            'censusList[0].number': json.data.data.visitedNum,
            'censusList[1].number': json.data.data.collectNum,
            'censusList[2].number': json.data.data.givelikeNum,
            memberId: json.data.data.memberId,
            erWeiMa: json.data.data.cardCodeUrl,
            objectId:json.data.data.objectId,
            objectName:json.data.data.objectName,
            avatarUrl:json.data.data.avatarUrl
          })
        
      },
      fail: function () {
        wx.showModal({
          title: '请求超时',
          content: '请重启小程序或联系数据库管理员',
          success: function (res) {
            if (res.confirm) {
            }
          }
        })
      }
    })
  },
  // building: function (latitude,longitude){
  //   wx.request({
  //     url: common.formal + '/side/getSide.form',
  //     data: {
  //       longitude:longitude,
  //       latitude:latitude,
  //       enterpriseIndustry:'2'

  //     },
  //     method: 'get',
  //     success: function (json) {
  //       console.log(json)
  //     }
  //   })
  // },
  toVr: function (e,vrTag,indetify) {
    if (vrTag==null||undefined||''){
      vrTag = e.currentTarget.dataset.vrtag
      indetify = e.currentTarget.dataset.indetify 
     
      console.log(indetify, vrTag)
      wx.reLaunch({
        url: '../index/index?unionid=' + this.data.unionid + "&vrTag=" + vrTag + "&objectId=" + this.data.objectId + '&imageUrl=' + this.data.imageUrl + '&identityStatus=' + indetify + '&objectType=' + '1' + "&meobjectId=" + this.data.meobjectId
      })
    }
    
  },
  buildingEvents:function(e){
    var _that = this
    let indetify = e.currentTarget.dataset.indetify
    let vrTag = e.currentTarget.dataset.vrtag
     // 类型判断开始
    if (indetify == '1') {
          // 企业基础版
      _that.toVr('e',vrTag, indetify);
          _that.setData({
          })
    } else if (indetify == '2') {
          // 企业升级版
      _that.toVr('e',vrTag, indetify);
          _that.setData({
          })
        }
  },
  enterprise: function (latitude,longitude){
    var that = this
    wx.request({
      url: common.formal + '/side/getSide.form',
      data: {
        longitude:longitude,
        latitude: latitude,
        enterpriseIndustry:'1'
      },
      method: 'get',
      success: function (json) {
        console.log(json.data.data)
        wx.hideLoading()
        that.setData({
          enterpriseNums:json.data.data
        })
      }
    })
  },
  onLoad: function (options) {
    var _that = this
    _that.setData({
      condition:5,
      times:_that.data.times++,
    })
    wx.getLocation({
      type: 'wgs84',// 默认wgs84
      success: function (res) {
        // _that.building(res.latitude, res.longitude)
        _that.enterprise(res.latitude, res.longitude)
      },
      fail: function (res) {
        _that.globalData.longitude = '120.202574'
          _that.globalData.latitude = '30.27604'
        
      }
    });
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    if(options!=undefined&&options.bindphone){
      _that.setData({
        binds: 1
      })
    }
    if (app.globalData.unionid && app.globalData.unionid != '') {
      let unionid = app.globalData.unionid;
      _that.setData({
        unionid: unionid
      })
      _that.taCardInit(unionid)
    } else {
      app.unionidCallback = unionid => {
        if (unionid != '') {
          _that.setData({
            unionid: unionid
          })
          _that.taCardInit(unionid)
        }
      }
    }
    wx.getSystemInfo({
      success: function (res) {
        // 获取可使用窗口宽度
        let clientHeight = res.windowHeight;
        // 获取可使用窗口高度
        let clientWidth = res.windowWidth;
        // 算出高度(单位rpx)
        let height = clientHeight
        // 设置高度
        _that.setData({
          height: height
        })
      },
    });
   },
   onShow:function(){ 
   },
  onShareAppMessage: function (res) {
    var _this = this;
    
    if (res.from === 'button') {
    }
    return {
      title: "您好，这是我的名片，望惠存",
      path: "pages/TaCard/TaCard?objectId=" + _this.data.objectId + '&vrTag=' +_this.data.vrTag + '&imageUrl=' + _this.data.imageUrl + '&memberId=' + _this.data.memberId,
      imageUrl: _this.data.imageUrl
    }
  }
})
