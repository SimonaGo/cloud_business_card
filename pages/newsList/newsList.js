// pages/newsList/newsList.js
const common = require('../../js/config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
      wrapperpositionAll:154,
      current:0,
      modal_img:'https://oss.wdsvip.com/image/tools/32ba0e64043f835eed74007ccde106a.png',
    modal_img_del:'https://oss.wdsvip.com/image/tools/f07b1f62bedd64143b5cd60de88c12b.png',
      showModal:true,
      pictures:'',
      positionList:"",
      productList:'',
      enterpriseName:'',
      objectInfo:'',
      img_background:'',
      isScroll:false,
      hei:''
  },
  swiperChange:function(e){
    this.setData({
      current:e.detail.current
    })
  },
  closeModal:function(){
    this.setData({
      showModal:false,
      isScroll:true
    })
    console.log(this.data.isScroll)
  },
  swiperClick:function(e){
    console.log(e.currentTarget.dataset);
    let _this=this
    let _index=_this.data.current;
    console.log('您点击了第' + _index + '张图片' + ':' + e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '../newsDetail/newsDetail?id='+e.currentTarget.dataset.id
    })
  },
  newsInit: function (vrTag) {
    var _that = this
    wx.request({
      url: common.formal + '/getProductList.form',
      data: {
        vrTag:vrTag,
        articleStatus: "1",
        articleType:'2' 
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        let imgArray=json.data.data;
        // json.data.data.forEach(x => imgArray.push(x.imageUrl+','+x.articleId))
        console.log(imgArray)
        if(imgArray.length!='0'&&imgArray!=''&&imgArray!='null')
        {
          _that.setData({
            pictures:imgArray,
            showpictures:true
          })  
        }else{
          _that.setData({
            pictures:imgArray,
            showpictures:false
          })  
        }
       
      }
    })
  },
  positionInit: function (vrTag) {
    var _that = this
    wx.request({
      url: common.formal + '/vrCard/getPositionList.form',
      data: {
        vrTag:vrTag,
        articleStatus: "1",
        articleType:'2' 
      },
      method: 'get',
      success: function (json) {
        console.log(json.data.data)
        // json.data.data.forEach(x => {
        //   x.ctime=x.ctime.substring(0,10)
        // });
        var positionList = json.data.data
        if(positionList.length!='0'&&positionList!=''&&positionList!='null')
        {
          _that.setData({
            positionList:json.data.data,
            showpositionList:true
          })  
        }else{
          _that.setData({
            positionList:json.data.data,
            showpositionList:false
          }) 
        }
       
      }
    })
  },
  allPositionAllInfor:function(){

  },
 productInit: function (vrTag) {
    var _that = this
    wx.request({
      url: common.formal + '/getProductList.form',
      data: {
        vrTag:vrTag,
        articleStatus: "1",
        articleType:'1' 
      },
      method: 'get',
      success: function (json) {
        var productList = json.data.data
        console.log(productList)
        if(productList.length !='0' &&productList!='' &&productList!='null')
        {
          _that.setData({
            productList:json.data.data,
            showproductList:true
          })  
        }else{
          _that.setData({
            productList:json.data.data,
            showproductList:false
          }) 
        }
      }
    })
  },
  imgheight:function(e){
    var winWid = wx.getSystemInfoSync().windowWidth;         //获取当前屏幕的宽度
    var imgh=e.detail.height;　　　　　　　　　　　　　　　　//图片高度
    var imgw=e.detail.width;
    var swiperH=winWid*imgh/imgw + "px"　　　　　　　　　　//等比设置swiper的高度。  即 屏幕宽度 / swiper高度 = 图片宽度 / 图片高度    ==》swiper高度 = 屏幕宽度 * 图片高度 / 图片宽度
    this.setData({
        Hei:swiperH　　　　　　　　//设置高度
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _that = this
    console.log(options)
    _that.setData({
      enterpriseName:options.enterpriseName,
      objectInfo:options.objectInfo
    })
    wx.getSystemInfo({
      success: function (res) {
        // 获取可使用窗口宽度
        let clientHeight = res.windowHeight;
        // 获取可使用窗口高度
        let clientWidth = res.windowWidth;

        // 算出高度(单位rpx)
        let height = clientHeight
        // 设置高度
        _that.setData({
          height: height
        })
        console.log(_that.data.height)
      },
    });
    _that.newsInit(options.vrTag)
    _that.positionInit(options.vrTag)
    _that.productInit(options.vrTag)

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})