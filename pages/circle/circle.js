
//获取应用实例
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
var WxParse = require('../../dist/wxParse.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    numbers:1,
    number:1,
    show:true,
    shows: true,
    ready:false,
    zan:6,
    fabulousId:null,
    citys:['1','2','3','4','5','6'],
    fabulous:[],
    allLikes:0,
    selectArea:0
  },
  toresumecenter(e){
    const where =  e.currentTarget.dataset.where
    wx.navigateTo({
      url: '../'+where+'/'+where,
    })
  },
  countGiveLikeNum: function (e) {
    var _that = this;
    wx.request({
      // url: common.domain + '/mini/giveLikeToCard.form',
      url: common.formal + '/card/countGiveLikeNum.form',
      // url: 'https://minsu.vryundian.com:6449/card/giveLikeToCard.form',
      data: {
        objectId: e.currentTarget.dataset.id
      },
      method: 'get',
      success: function (json) {
        _that.setData({
          zan: 6
        })
        wx.showToast({
          title: '取消点赞',
        })
        // if (json.data.status == '1') {
        //   _that.setData({
        //     'censusList[2].number': parseInt(_that.data.censusList[2].number) + 1
        //   })
        // } else if (json.data.status == '2') {
        //   wx.showToast({
        //     title: '已点过赞',
        //   })
        // }
      }
    })
  },
  deleteGiveLike: function(e) {
    var _that = this;
    let fabulous = e.currentTarget.dataset.fabulous
    let givelike = e.currentTarget.dataset.givelike
    let index = parseInt(e.currentTarget.dataset.index)
    let changefabulous = '' + fabulous + '[' + index + ']'
    let changegivelikes = '' + givelike + '[' + index + ']'
    if (givelike == 'givelike') {
      var aa = _that.data.givelike[index]
    } else {
      var aa = _that.data.givelikeOfpro[index]
    }
    var _that = this;
    wx.request({
      url: common.formal + '/card/deleteGiveLike.form',
      data: {
        objectId: e.currentTarget.dataset.id,
        unionid: _that.data.unionid,
        test: common.test
      },
      method: 'get',
      success: function (json) {
        _that.setData({
          [changefabulous]: null,
          [changegivelikes]: parseInt(aa) - 1
        })
      }
    })
  },
  giveLikeToCard: function (e) {
    let index = e.currentTarget.dataset.index
    let fabulous = e.currentTarget.dataset.fabulous
    let givelike = e.currentTarget.dataset.givelike
    let changefabulous = ''+fabulous+'['+index+']'
    let changegivelikes = ''+givelike+'[' + index + ']'
    var _that = this;
    if (givelike =='givelike'){
      var aa = _that.data.givelike[index]
    }else{
      var aa = _that.data.givelikeOfpro[index]
    }
    wx.request({
      url: common.formal + '/card/giveLikeToCard.form',
      data: {
        objectId: e.currentTarget.dataset.id,
        unionid: _that.data.unionid,
        test: common.test
      },
      method: 'get',
      success: function (json) {
        _that.setData({
          [changefabulous]: 'null',
          [changegivelikes]: parseInt(aa)+1
        })
      }
    })
  },
  enterprise: function () {
    var that = this
    wx.request({
      url: common.formal + '/side/getSide.form',
      data: {
        longitude: this.data.longitude,
        latitude: this.data.latitude,
        enterpriseIndustry: '1'
      },
      method: 'get',
      success: function (json) {
        that.setData({
          enterpriseNums: json.data.data
        })
      }
    })
  },
  moreInfors:function(){
    this.setData({
      numbers: this.data.imgArrayLength,
      show:false
    })
  },
  reduceInfors:function(){
    this.setData({
      numbers: 1,
      show: true,
      selectArea:1
    })
  },
  moreInfor: function () {
    this.setData({
      number: this.data.productListLength,
      shows: false
    })
  },
  reduceInfor: function () {
    this.setData({
      number: 1,
      shows: true
    })
  },
  taCardInit: function (unionid) {
    var _that = this
    wx.request({
      url: common.formal + '/transInit.form',
      data: {
        unionid: unionid,
        contactList: false,
        start: 0,
        end: 1
      },
      method: 'get',
      success: function (json) {
        _that.setData({
          companyName: json.data.data.enterpriseName,
          unionid:unionid
        })
      }
    })},
  newsInit: function () {
    var _that = this
    wx.request({
      url: common.formal + '/articleZW/selectAllArticleZW.form',
      data: {
        articleType: '2',
        start: 0,
        unionid: _that.data.unionid,
        end: 5
      },
      method: 'get',
      success: function (json) {
        let imgArray = json.data.data;
        if (imgArray.length != '0' && imgArray != '' && imgArray != 'null') {
          _that.setData({
            pictures: imgArray,
            showpictures: false,
            imgArrayLength: imgArray.length
          }) 
          } else {
          _that.setData({
            pictures: imgArray,
            showpictures: false
          })
        }
        let fabulous = new Array()
        let givelike = new Array()
        let list={}
          for (let i = 0; i < imgArray.length; i++ ){
            fabulous.push(imgArray[i].fabulousId)
            givelike.push(imgArray[i].giveLikeNum)
          }
          _that.setData({
            fabulous: fabulous,
            givelike: givelike
          })
        
      }
    })
  },
  positionInit: function (vrTag) {
    var _that = this
    wx.request({
      url: common.formal + '/vrCard/selectPositionList.form',
      data: {
       start:0,
       end:10
      },
      method: 'get',
      success: function (json) {
        // json.data.data.forEach(x => {
        //   x.ctime=x.ctime.substring(0,10)
        // });
        var positionList = json.data.data
        if (positionList.length != '0' && positionList != '' && positionList != 'null') {
          _that.setData({
            positionList: json.data.data,
            showpositionList: true,
            alldata: json.data.data.length
          })
        } else {
          _that.setData({
            positionList: json.data.data,
            showpositionList: false
          })
        }

      }
    })
  },
  productInit: function (vrTag) {
    var _that = this
    wx.request({
      url: common.formal + '/articleZW/selectAllArticleZW.form',
      data: {
        articleType: '1',
        start:0,
        unionid: _that.data.unionid,
        end:5
      },
      method: 'get',
      success: function (json) {
        var now = new Date();
        _that.setData({
          ready: true
        })
        wx.hideLoading()
        var productList = json.data.data
        var dateTimeStamp = json.data.data.ctime
        if (productList.length != '0' && productList != '' && productList != 'null') {
          _that.setData({
            productList: json.data.data,
            showproductList: true,
            productListLength: productList.length
          })
        } else {
          _that.setData({
            productList: json.data.data,
            showproductList: false
          })
        }
        let fabulous = new Array()
        let givelike = new Array()
        for (let i = 0; i < productList.length; i++) {
          fabulous.push(productList[i].fabulousId)
          givelike.push(productList[i].giveLikeNum)
        }
        _that.setData({
          fabulousOfpro: fabulous,
          givelikeOfpro: givelike
        })
      }
    })
  },
/**
 * 导航页面显示2）
 */
swiperChange: function (e) {
  this.setData({
    currentTab: e.detail.current,
  })
},
lower: function (e) {
  if (this.data.loadmoreData){
  
    this.loadmorenav();
  }else{
    this.setData({
      loadmore: 'false',
    })
  }
},
  toposition:function(e){
    wx.navigateTo({
      url: './../positionDetail/positionDetail?id=' + e.currentTarget.dataset.id + '&vrtag=' + e.currentTarget.dataset.vrtag,
    })
  },
  toenterprise:function(e){
    wx.navigateTo({
      url: './../enterpriseCard/enterpriseCard?id=' + e.currentTarget.dataset.enterpriseid + '&vrtag=' + e.currentTarget.dataset.vrtag,
    })
  },
  // 点击切换当前页时改变样式
  swichNav: function (e) {
    var that = this,
        cur = e.target.dataset.current,
        src = e.target.dataset.src;
    if (that.data.currentTaB == cur) {
      return false;
    } else {
      that.setData({
        currentTab: cur
      })
    }
    that.setData({
      cardImgSrc: src
    })
    that.checkCor();
  },
  tonewsDetail:function(e){
    setTimeout(function(){
      wx.navigateTo({
        url: './../newsDynamicDetail/newsDynamicDetail?id=' + e.currentTarget.dataset.id + '&title=' + e.currentTarget.dataset.title + '&vrTag=' + e.currentTarget.dataset.vrtag + '&flag=' + e.currentTarget.dataset.flag + '&intro=' + e.currentTarget.dataset.intro
      })
    },400)
   
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    let _that = this
    _that.enterprise()
   
    if (app.globalData.unionid && app.globalData.unionid != '') {
      let unionid = app.globalData.unionid;
      _that.setData({
        unionid: unionid
      })
      _that.taCardInit(unionid)
      _that.positionInit()
      _that.newsInit()
      _that.productInit()
    } else {
      app.unionidCallback = unionid => {
        if (unionid != '') {
          _that.setData({
            unionid: unionid
          })
          _that.taCardInit(unionid)
        }
        _that.positionInit()
        _that.newsInit()
        _that.productInit()
      }
  }
  },
  onShareAppMessage: function (res) {
    var _this = this;
    console.log(res.target, res.target.dataset.pare, res.target.dataset.imag)
    if (res.from === 'button') {
    }
    return {
      title: res.target.dataset.pare,
      path: "pages/newsDynamicDetail/newsDynamicDetail?unionid=" + _this.data.unionid + '&vrTag=' + res.target.dataset.vrTag + '&flag=' + res.target.dataset.flag + '&title=' + res.target.dataset.title +'&id=' + res.target.dataset.id,
      imageUrl: res.target.dataset.imag
    }
  }
})