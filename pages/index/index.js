
const app = getApp();
const common = require('../../js/config.js');
// const author = require('../../js/author.js');
var para = '';
const updateManager = wx.getUpdateManager()
updateManager.onUpdateReady(function () {
  wx.showModal({
    title: '更新提示',
    content: '新版本已经准备好，是否马上重启小程序？',
    success: function (res) {
      if (res.confirm) {
        updateManager.applyUpdate()
      }
    }
  })
})
Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    vrTag: '',
    displays: 'none',
    unide: '',
    imgurls: '',
    objectId: '',
    vrurl:''
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  toViewUrl: function () {
    var _this = this;
    var toUrl = _this.options.toUrl.replace('\+\+\+', '?');
    toUrl = toUrl.replace(/---/g, '=');
    toUrl = toUrl.replace(/\*\*\*/g, '&');
    var toUrl = toUrl + '&userInfoStatus=true&unionid=' + _this.options.unionid + '&proTag=miniwx&miniOpenid=' + _this.options.openId + '&vrTag=' + common.vrTag + '&enter=' + common.enter + '&test=' + common.test;
    _this.setData({
      vrurl: toUrl
    });
  },
  getMemberMsg:function(vrTag,unid){
    var _this = this
    wx.request({
      url: common.formal +'/member/confine/getMemberMsg.form',
      // url: 'https://minsu.vryundian.com:6449/member/confine/getMemberMsg.form',
      data: {
        unionid: unid,
        test: common.test,
        vrTag: vrTag
      },
      method: 'get',
      success: function (json) {
        console.info(json)
        _this.setData({
          memberId:json.data.memberId,
          phone: json.data.phone,
          nickName: json.data.nickName,
          enterprise: json.data.enterpriseName,
          position: json.data.position,
          visitedNum: json.data.visitedNum,
          collectNum: json.data.collectNum,
          giveLikeNum: json.data.givelikeNum,
          // vrurl: 'https://oss.wdsvip.com/c/'+vrTag+'/index.html?userInfoStatus=true&unionid=' + unid + '&proTag=miniwx&vrTag=' + vrTag + '&enter=' + common.enter + '&test=' + common.test
        })

      }
    })
  },
  checkMember: function (vrTag, unionid){
    wx.request({
      url: common.formal + '/member/checkMember.form',
      // url: 'https://minsu.vryundian.com:6449/member/checkMember.form',
      data: {
        test: common.test,
        vrTag: vrTag,
        unionid: unionid
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        if (json.data.status == "1") {
          wx.reLaunch({
            url: '../bindPhone/bindPhone?vrTag=' + vrTag + "&unionid=" + unionid
          })
        } else {
          wx.reLaunch({
            url: '../trans/trans'
          })
        }
      }
    })
  },
  getShareCardUr: function (vrTag, unid){
    var _this = this;
    wx.request({
      // url: common.domain + '/getShareCardUrl.form',
      url: common.formal +'/card/getShareCardUrl.form',
      // url: 'https://minsu.vryundian.com:6449/card/getShareCardUrl.form',
      data: {
        unionid: unid,
        test: common.test,
        vrTag: vrTag
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        if (json.data != "" && json.data.data.imageUrl != null) {
          _this.setData({
            imgurls: json.data.data.imageUrl,
            objectId: json.data.data.objectId
          })
        }
        if (_this.options.imageUrl != '' && _this.options.imageUrl != undefined && _this.options.imageUrl != null) {
      
          _this.setData({
            imgurls: _this.options.imageUrl,
            objectId: _this.options.objectId
          })
        }
      },
    })
  },
  onLoad: function (options) {
    var _that=this;
    console.log(options)
    if (options.scene){
      if (app.globalData.unionid && app.globalData.unionid != '') {
        let unionid = app.globalData.unionid;
        console.log(unionid)
        _that.setData({
          unionid: unionid
        })
        this.getMemberMsg(options.scene, unionid)
        this.getShareCardUr(options.scene, unionid)
        this.checkMember(options.scene, unionid)
      } else {
        app.unionidCallback = unionid => {
          if (unionid != '') {
            _that.setData({
              unionid: unionid
            })
            this.getMemberMsg(options.scene, unionid)
            this.getShareCardUr(options.scene, unionid)
            this.checkMember(options.scene, unionid)
          }
        }
      }
    }else{
      this.getMemberMsg(options.vrTag, options.unionid)
      console.log('https://oss.wdsvip.com/c/' + options.vrTag + '/index.html?userInfoStatus=true&unionid=' + options.unionid + '&proTag=miniwx&vrTag=' + options.vrTag + '&objectId=' + options.objectId + '&enter=' + common.enter + '&test=' + common.test + "&identityStatus=" + options.identityStatus + "&objectType=" + options.objectType + "&meobjectId=" + options.meobjectId)
      this.setData({
        imgurls: options.imageUrl,
        vrTag: options.vrTag,
        objectId: options.objectId,
        vrurl: 'https://oss.wdsvip.com/c/' + options.vrTag + '/index.html?userInfoStatus=true&unionid=' + options.unionid + '&proTag=miniwx&vrTag=' + options.vrTag + '&objectId=' + options.objectId + '&enter=' + common.enter + '&test=' + common.test + "&identityStatus=" + options.identityStatus + "&objectType=" + options.objectType + "&meobjectId=" + options.meobjectId
        // vrurl: 'https://wh.wdsvip.com/c/c0011/index.html?userInfoStatus=true&unionid=' + options.unionid + '&proTag=miniwx&vrTag=' + options.vrTag + '&objectId=' + options.objectId + '&enter=' + common.enter + '&test=' + common.test + "&identityStatus=" + options.identityStatus + "&objectType=" + options.objectType + "&meobjectId=" + options.meobjectId
      })
    }
  },
  onShareAppMessage: function (res) {
    var _this = this;
    var urls = res.webViewUrl
    console.log(res)
    if (res.from === 'button') {
    }
    return {
      title: "您好，这是我的名片，望惠存",
      path: "pages/TaCard/TaCard?objectId=" + _this.data.objectId + '&vrTag=' + _this.data.vrTag + '&imageUrl=' + _this.data.imgurls + '&memberId=' + _this.data.memberId,
      imageUrl: _this.data.imgurls
    }
  }
})
