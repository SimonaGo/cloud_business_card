//index.js
//获取应用实例
var tcity = require("../../utils/city.js");
const util = require('../../utils/util');

var app = getApp()
Page({
  data: {
    condition1: false,
    condition2: false,
    condition3:false,
    condition4: false,
    major:'',
    degree: '',
    degree1:'',
    minDate1: new Date().setDate(new Date().getDate()-3650*2),
    maxDate2: new Date().setDate(new Date().getDate()+365*4),
    minDate: new Date().setDate(new Date().getDate() - 3650 * 2),
    maxDate: new Date().setDate(new Date().getDate() + 365 * 4),
    columns1: ['高中', '专科', '本科', '研究生', '博士生'],
    columns2: ['学士', '硕士', '博士', '博士后']
  },
  onInput(event) {
    console.log(event.detail)
    this.setData({
      currentDate: util.formatTimeTwo(event.detail)
    });
  },
  onInput1(event) {
    console.log(event.detail)
    this.setData({
      startDate: util.formatTimeTwo(event.detail)
    });
  },
  onInput2(event) {
    console.log(event.detail)
    this.setData({
      endDate: util.formatTimeTwo(event.detail)
    });
  },
  onconfirm4(event) {
   this.setData({
     condition4:false
   })
  },
  onconfirm3(event) {
    this.setData({
      condition3: false
    })
  },
  oncancel4(event) {
    this.setData({
      condition4: false
    })
  },
  oncancel3(event) {
    this.setData({
      condition3: false
    })
  },
  onChange1(event) {
    const { picker, value, index } = event.detail;
    this.setData({
      degree1: `${value}`,
    })
  },
  onChange2(event) {
    const { picker, value, index } = event.detail;
    this.setData({
      degree: `${value}`
    })
  },
  oncancel() {
    this.setData({
      condition: !this.data.condition
    })
  },
  open: function () {
    this.setData({
      condition: !this.data.condition,
      condition1: false,
      condition2: false,
    })
  },
  open1: function () {
    this.setData({
      condition3: !this.data.condition3,
      condition1: false,
      condition2: false,
      condition4: false,
    })
  },
  open2: function () {
    this.setData({
      condition3: false,
      condition4: !this.data.condition4,
      condition1: false,
      condition2: false,
    })
  },
  open_van1: function () {
    this.setData({
      condition1: !this.data.condition1,
      condition2: false,
      condition3: false,
      condition4: false,
    })
  },
  open_van2: function () {
    this.setData({
      condition2: !this.data.condition2,
      condition1: false,
      condition3: false,
      condition4: false
    })
  },
  onLoad: function () {
  }
})
