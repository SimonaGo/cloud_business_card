// pages/TaCard/TaCard.js
const common = require('../../js/config.js');
const jsonjs = require('../../js/json2.js');
const app = getApp();
Page({
  data: {
    objectId: '',
    vrTag: '',
    showOrmiss: false,
    unionid:'',
    memberId:'', 
    condition: 0,
    checkAllPosition:'查看全部',
    companyPicture:'../../img/companyIntroduction.png',
    'companyInfor': [],
    img_url: 'http://a.wdsvip.com/vrCard/enterprise/wds.png',
    src: '../../img/ps.png',
    imagemore:"../../img/gengduo.png",
    mobilePhone:'',
    whatPages:'123',
    identityStatus:'',
    objectType:'',
    toCardDetail:'您还没有名片，点击可获取自己的名片',
    blockTagTa:'base',
    loadall:false,
    'iconList': [
      {
        img: '../../img/dianhua.png',
        event: "callCusPhone",
      },
      {
        img: '../../img/dizhi.png',
        event: 'gotoMap',
      },
      {
        img: '../../img/fenxiang.png',
        type: 'share',
        tag: 'button',
        event: ''
      }
    ],
    'censusList': [
      {
        type: "访客",
        number: 0,
        img_url:'../../img/yanjing1.png'
      },
      {
        type: "标星",
        number: 0,
        event:'addCardToBag',
        img_url:"../../img/xingx1.png"
      },
      {
        type: "点赞",
        number: 0,
        event: 'giveLikeToCard',
        img_url:"../../img/dianzan1.png"
      }
    ]
  },
  inforInit: function (vrTag) {
    var _that = this
    wx.request({
      url: common.formal + '/enterprise/getEnterpriseMsg.form',
      data: {
        vrTag: vrTag,
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        if(json.data.msg=='成功'){
          _that.setData({
            companyBriefInfor:json.data.data.briefInfo,
            hasInfor:true
          })
        }else{
          _that.setData({
            companyBriefInfor: '暂未发布相关消息！',
            hasInfor: false
          })
        }
      }
    })
  },
  gotoMap: function () {
    var _that = this
    wx.request({
      url: common.formal +'/enterprise/getLongitudeAndLatitude.form',
      data:{
        objectId:_that.data.objectId
      },
      method:'get',
      success:function (res){
        console.log(res.data.data)
        let plugin = requirePlugin('routePlan');
        let key = 'EWDBZ-LNTW6-OBISF-M7XFS-RJVTF-ELB3L';  //使用在腾讯位置服务申请的key
        let referer = 'vr云名片';   //调用插件的app的名称
        let endPoint = JSON.stringify({  //终点
          'name': res.data.data.enterpriseName,
          'latitude': res.data.data.latitude,
          'longitude': res.data.data.longitude
        });
        wx.navigateTo({
          url: 'plugin://routePlan/index?key=' + key + '&referer=' + referer + '&endPoint=' + endPoint
        });
      }
    })
  },
  allPosition: function () {
    console.log(this.data.showOrmiss)
    if (this.data.showOrmiss) {
      this.setData({
        condition: 3,
        showOrmiss: !(this.data.showOrmiss),
        checkAllPosition: "查看全部"
      });
      console.log(this.data.condition, this.data.here)
    } else {
      console.log(123)
      this.setData({
        condition: this.data.alldata,
        showOrmiss: !(this.data.showOrmiss),
        checkAllPosition: "收起全部"

      });
      console.log(this.data.condition, this.data.checkAllPosition)
    }
    console.log(this.data.showOrmiss, this.data.checkAllPosition)
  },
  GoTrans:function(){
   wx.reLaunch({
    url: './../trans/trans'
   })
  },
  giveLikeToCard:function(){
    var _that=this;
    wx.request({
      // url: common.domain + '/mini/giveLikeToCard.form',
      url: common.formal+'/card/giveLikeToCard.form',
      // url: 'https://minsu.vryundian.com:6449/card/giveLikeToCard.form',
      data:{
        objectId: this.data.objectId,
        unionid: _that.data.unionid,
        test: common.test
      },
       method: 'get',
      success: function (json) {
        if (json.data.status == '1') {
          _that.setData({
            'censusList[2].number': parseInt(_that.data.censusList[2].number) + 1
          })
        } else if (json.data.status == '2') {
          wx.showToast({
            title: '已点过赞',
          })
        }
      }
    })
  },
  addVisitedNum: function (objectId, memberId, test) {
    var _that = this;
    console.log("objectId:" + objectId + "memberId:" + memberId + "test:" + test, common.formal )
    wx.request({
      // url: common.domain + '/mini/giveLikeToCard.form',
      url: common.formal +'/card/addVisitedNum.form',
      // url: 'https://minsu.vryundian.com:6449/card/addVisitedNum.form',
      data: {
        objectId: objectId,
        unionid: _that.data.unionid,
        test: test
      },
      method: 'get',
      success: function (json) {
        if (json.data.status == '1') {
          _that.setData({
            'censusList[0].number': parseInt(_that.data.censusList[0].number) + 1
          })
        }
      }
    })
  },
  addCardToBag: function () {
   var _that=this;
    wx.request({
      // url: common.domain + '/mini/addCardToBag.form',
      url: common.formal +'/card/addCardToBag.form',
      // url: 'https://minsu.vryundian.com:6449/card/addCardToBag.form',
      data: {
        objectId: _that.data.objectId,
        unionid: _that.data.unionid,
        test: common.test
      },
      method: 'get',
      success: function (json) {
        if (json.data.status == '1') {
          _that.setData({
            'censusList[1].number': parseInt(_that.data.censusList[1].number) + 1
          })
        } else if (json.data.status == '2') {
          wx.showToast({
            title: '您已收藏过',
          })
        }
      }
    })
  },
  callCusPhone: function () {
    wx.makePhoneCall({
      phoneNumber:this.data.mobilePhone ,
      success: function () {
        console.log("拨打电话成功！")
      },
      fail: function () {
        console.log("拨打电话失败！")
      }
    })
  },
  toVr: function () {
    console.log(this.data.unionid, this.data.meobjectId)
    wx.reLaunch({
      url: '../index/index?unionid=' + this.data.unionid + "&vrTag=" + this.data.vrTag + "&objectId=" + this.data.objectId + '&imageUrl=' + this.options.imageUrl + '&identityStatus=' + this.data.identityStatus + '&objectType=' + this.data.objectType + "&meobjectId=" + this.data.meobjectId
    })
  },
  getSentenceinfor: function (objectId) {
    var _that = this
    wx.request({
      url: common.formal + '/selectSentence.form',
      method: 'get',
      success: function (json) {
        _that.setData({
          Sentenceinfor:json.data.data.sentenceContent
          })
        }
      })
    },
    toList: function (whatPages) {
      var whatPages =whatPages.currentTarget.dataset.whatpages
      console.log(whatPages)
      wx.navigateTo({
        url: '../' + whatPages + '/' + whatPages + '?objectId=' + this.data.meobjectId + '&vrTag=' + this.data.vrTag + '&objectInfo=' + this.data.objectInfo + '&enterpriseName=' + this.data.enterpriseName + '&unionid=' + this.data.unionid+'&imageUrl=' + this.data.imageUrl+'&identityStatus='+ this.data.identityStatus+'&objectType='+this.data.objectType
      })
    },
    tourist:function(unionid){
      console.log(unionid)
      var _that = this
      wx.request({
        url: common.formal + '/transInit.form',
        data: {
          unionid: unionid,
          start:0,
          end:10
        },
        method:'get',
        success:function(json){
          console.log(json)
          console.log('游客身份识别成功')
          if (json.data.status == '3' || json.data.status == '4'){
              _that.setData({
                toCardDetail: '您还没有名片，点击可获取自己的名片',
                whatPages:'base_card_addition',
                identityStatus:json.data.data.identityStatus,
                objectType:json.data.data.objectType
            })
          }
          else{
            console.log('开始分发环节')
            // 企业基础版
            _that.setData({
              toCardDetail: '进入我的主名片',
              whatPages: 'carDetail',
              meobjectId: json.data.data.objectId,
              objectInfo: json.data.data.objectInfo,
              enterpriseName: json.data.data.enterpriseName,
              unionid: json.data.data.unionid,
              imageUrl: json.data.data.imageUrl,
              identityStatus: json.data.data.identityStatus,
              objectType: json.data.data.objectType
            })
            // _that.taCardInit(objectId,vrTag, imageUrl)
          }
        }
      })
    },
    taCardInit: function (objectId,vrTag, imageUrl){
     var _that = this
      let unionid = _that.data.unionid
      console.log(objectId);
      console.log(vrTag);
      wx.request({
      url: common.formal +'/taCardInit.form',
      data: {
        objectId: objectId,
        imageUrl: imageUrl
       },
      method: 'get',
      success: function (json) {
        console.log(json)
        _that.setData({
          objectIdForShare:json.data.data.objectId,
          vrTagForShare: json.data.data.vrTag,
          imageUrlForShare: json.data.data.imageUrl,
          loadall:true
        })
        var test = common.test;
         _that.addVisitedNum(objectId,json.data.data.unionid, test);
        if (objectId!=undefined&&objectId.indexOf("base_")!=-1){
          _that.setData({
            blockTagTa:'base'
          })
         }else{
          if (vrTag.indexOf("mf_")!=-1){
          //
           }else{
            _that.setData({
              blockTagTa: 'fbase'
            })
           }
         }
        _that.setData({
           "companyInfor[0]": json.data.data,
           'censusList[0].number': json.data.data.visitedNum,
           'censusList[1].number': json.data.data.collectNum,
           'censusList[2].number': json.data.data.givelikeNum, 
            mobilePhone:json.data.data.phone,
            img_url: json.data.data.enterpriseUrl,
            objectId: json.data.data.objectId,
            memberId: json.data.data.memberId,
            identityStatus:json.data.data.identityStatus,
            enterpriseName:json.data.data.enterpriseName,
            objectInfo:json.data.data.objectInfo,
            objectType:json.data.data.objectType,
            identityStatus:json.data.data.identityStatus
        })
        wx.hideLoading();
        console.log(_that.data.unionid)
        _that.tourist(_that.data.unionid)  //开始路径分发
        _that.positionInit(_that.data.vrTagForShare) //获取招聘信息
      }
    })
  },
  taCardInitAgain: function (options){
    console.log('进入cardNo换取字段环节')
    var _that = this
   wx.request({
     url: common.formal +'/getInfoByCardNo.form',
     data: {
      cardNo:options
     },
     method: 'get',
     success: function (json) {
       console.log('换取字段成功')
       console.log(json.data.data)
       if (json.data.data==null){
         wx.hideLoading()
          wx.showModal({
            title: '错误！',
            content: '当前卡片已删除或已销毁',
            success: function (res) {
              if(res.confirm) {
                wx.showToast({
                  title: '请查看其他名片',
                  icon: 'none',
                  duration: 5000
                })
              }
            }
          })
       }else{
         _that.setData({
           objectId: json.data.data.objectId,
           vrTag: json.data.data.vrTag,
           imageUrl: json.data.data.imageUrl
         })
         _that.taCardInit(json.data.data.objectId, json.data.data.vrTag, json.data.data.imageUrl)
         console.log('进入了当前页面对象个人信息初始化环节')
        
       }
       
     }
   })
 },
  taCardOnLoad: function (options,unionid){
    var _that = this;
    console.log(unionid)
    console.log(options.scene)
    if(unionid != "" && options.scene != undefined &&
      options.scene.indexOf('cardNo') != -1) {//老用户扫名片码进入入口
      console.log('老用户进入了cardNo环节')
      var num = options.scene.replace('cardNo', '')
      _that.taCardInitAgain(num);
    } else if (options.cardNo != undefined) {//新用户扫名片码进入入口
      console.log('新用户进入cardNo环节')
      var num = options.cardNo
      _that.taCardInitAgain(num);
    } else {//头部获取参数
      _that.setData({
        objectId: _that.options.objectId,
        vrTag: _that.options.vrTag,
        imageUrl: _that.options.imageUrl
      })
      _that.taCardInit(options.objectId, options.vrTag, options.imageUrl)
      console.log('进入了当前页面对象个人信息初始化环节')
     
    }
    console.log(unionid)
  },
  positionInit: function (vrTag) {
        var _that = this
        wx.request({
            url: common.formal + '/vrCard/getPositionList.form',
            data: {
                vrTag: vrTag,
                articleStatus: "1",
                articleType: '2'
            },
            method: 'get',
            success: function (json) {
                console.log(json.data.data)
                // json.data.data.forEach(x => {
                //   x.ctime=x.ctime.substring(0,10)
                // });
                var positionList = json.data.data
                if (positionList.length != '0' && positionList != '' && positionList != 'null') {
                    _that.setData({
                        positionList: json.data.data,
                        showpositionList: true,
                        alldata:json.data.data.length
                    })
                } else {
                    _that.setData({
                        positionList: json.data.data,
                        showpositionList: false
                    })
                }

            }
        })
    },
  allPositionAllInfor(e){
    wx.navigateTo({
      url: './../positionDetail/positionDetail?id=' + e.currentTarget.dataset.id + '&vrtag=' + e.currentTarget.dataset.vrtag,
    })
  },
 
  onLoad: function (options) {
    console.log(options)
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    this.inforInit(options.vrTag)//公司简介
    wx.getLocation({
      type:'wgs84',// 默认wgs84
      success:function(res){
        console.log(res)
      },
      fail:function(res){},
      complete:function(){}
  });
    var _that = this
   
    this.getSentenceinfor()//每日一句
    console.log(options)
    if (app.globalData.unionid && app.globalData.unionid != ''){
      let unionid = app.globalData.unionid;
      console.log(unionid)
      _that.setData({
        unionid: unionid,
        condition:3
      })
      _that.taCardOnLoad(options, unionid)
    }else{
      app.unionidCallback = unionid => {
        if (unionid != '') {
          _that.setData({
            unionid: unionid,
            condition:3
          })
          _that.taCardOnLoad(options, unionid)
        }
      }
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var _this = this;
    console.log(_this.data.objectIdForShare, _this.data.vrTagForShare, _this.data.imageUrlForShare);
    if (res.from === 'button') {
    }
    return {
      title: "您好，这是我的名片，望惠存",
      path: "pages/TaCard/TaCard?objectId=" + _this.data.objectIdForShare + '&vrTag=' + _this.data.vrTagForShare + '&imageUrl=' + _this.data.imageUrlForShare,
      imageUrl: _this.data.imageUrlForShare
    }
  }
})