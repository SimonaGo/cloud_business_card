// pages/newsCenter/newsCenter.js
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  Goback: function () {
    wx.navigateBack({
      delta: 1
    })
  },
  inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },
  hideInput: function () {
    this.setData({
      inputVal: "",
      inputShowed: false
    });
  },
  clearInput: function () {
    this.setData({
      inputVal: ""
    });
  },
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  elsenewsInit: function (flag) {
    var _that = this
    wx.request({
      url: common.formal + '/articleZW/selectAllArticleZW.form',
      data: {
        articleType: 1,
        start: 0,
        end: 5
      },
      method: 'get',
      success: function (json) {
        let elsenewsArray = json.data.data;
        if (elsenewsArray.length != '0' && elsenewsArray != '' && elsenewsArray != 'null') {
          _that.setData({
            elsenewsArray: elsenewsArray
          })
        } else {
          _that.setData({
            elsenewsArray: elsenewsArray
          })
        }
      }
    })
  },
  // 获取滚动条当前位置
  onPageScroll: function (e) {
    console.log(e)
    if (e.scrollTop > 100) {
      this.setData({
        floorstatus: true
      });
    } else {
      this.setData({
        floorstatus: false
      });
    }
  },

  //回到顶部
  goTop: function (e) {  // 一键回到顶部
    if (wx.pageScrollTo) {
      wx.pageScrollTo({
        scrollTop: 0
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.elsenewsInit()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})