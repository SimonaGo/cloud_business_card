//index.js
//获取应用实例
var tcity = require("../../utils/city.js");
const util = require('../../utils/util');

var app = getApp()
Page({
  data: {
    condition: false,
    condition1: false,
    condition2: false,
    times: '',
    skillAbility: '',
    columns: ['半年以下', '1年', '2年', '3年','3年以上'],
    columns1: ['普通', '良好', '熟练', '精通']
  },
  onInput(event) {
    console.log(event.detail)
    this.setData({
      currentDate: util.formatTimeTwo(event.detail)
    });
  },
  onConfirm(event) {
    const { picker, value, index } = event.detail;
    Toast(`当前值：${value}, 当前索引：${index}`);
  },
  onChange1(event) {
    const { picker, value, index } = event.detail;
    this.setData({
      times: `${value}`,
    })
  },
  onChange2(event) {
    const { picker, value, index } = event.detail;
    this.setData({
      skillAbility: `${value}`
    })
  },
  oncancel() {
    this.setData({
      condition: !this.data.condition
    })
  },
  open: function () {
    this.setData({
      condition: !this.data.condition,
      condition1: false,
      condition2: false,
    })
  },
  open1: function () {
    this.setData({
      condition: false,
      condition2: false,
    })
  },
  open_van1: function () {
    this.setData({
      condition2: !this.data.condition2,
      condition: false,
      condition1: false
    })
  },
  onLoad: function () {
    console.log(new Date().getTime())
    var that = this;
    tcity.init(that);
    var cityData = that.data.cityData;
    const provinces = [];
    const citys = [];
    const countys = [];
  }
})
