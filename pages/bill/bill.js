// pages/carDetail/carDetail.js
const common = require('../../js/config.js');
const author = require('../../js/author.js');
const util = require('../../utils/util');
var app = getApp();
Page({
  data: {
    dats:'',
    companyInfor: [
    ],
    friendList: [
    ],
    src: '../../img/ps.png',
    img_url: '',
    img_background: 'http://a.wdsvip.com/vrCard/enterprise/wds.png'
  },
  getinfor: function (objectId) {
    var _that = this
    wx.request({
      url: common.formal + '/objectTransInit.form',
      data: {
        objectId: objectId,
        colleagueList:"true",
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        console.log(json.data.data)
        console.log(json.data.data.colleagueList)
          _that.setData({
            "companyInfor[0]": json.data.data,
            friendList: json.data.data.colleagueList,
            vrTag: json.data.data.vrTag,
            objectId: json.data.data.objectId,
            imageUrl: json.data.data.imageUrl,
            hideLoadMore: false,
            showloadmore: true,
            alldata: json.data.data.colleagueList== null ? '0' : json.data.data.colleagueList.length,
            company: json.data.data.enterpriseName,
            'censusList[0].number': json.data.data.visitedNum,
            'censusList[1].number': json.data.data.collectNum,
            'censusList[2].number': json.data.data.givelikeNum,
            memberId: json.data.data.memberId,
            personsName:json.data.data.objectName
          })
        wx.hideLoading()
      },
      fail: function () {
        wx.showModal({
          title: '请求超时',
          content: '请重启小程序或联系数据库管理员',
          success: function (res) {
            if (res.confirm) {
            }
          }
        })
      }
    })
  },
  onLoad: function (options) {
    console.log(options)
    wx.showLoading({
      title: '加载中',
      mask:true,
    })
    var _that = this;
    let time = util.formatTime(new Date());  
    let date = util.getDates(1,time)
    date=[...date]
    time = util.splitarr(time)
    _that.setData({  
      time: time,
      dats:date[0].week
    });  
      var _that = this
    if (options.objectId){
      _that.setData({
        unionid: options.unionid
        })
      _that.getinfor(options.objectId);
    }
        
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.addCardToBag()
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var _this = this;
    console.log('sahre');
    if (res.from === 'button') {
    }
    return {
      title: "您好，这是我的名片，望惠存",
      path: "pages/TaCard/TaCard?objectId=" + _this.data.objectId + '&vrTag=' + _this.data.vrTag + '&imageUrl=' + _this.data.imageUrl + '&memberId=' + _this.data.memberId,
      imageUrl: _this.data.imageUrl
    }
  }
})