// pages/positionDetail/positionDetail.js
const common = require('../../js/config.js');
const jsonjs = require('../../js/json2.js');
const app = getApp();
//触屏开始点坐标
let startDot = {
  X: 0,
  Y: 0
};
//触屏到点坐标
let touchDot = {
  X: 0,
  Y: 0
};
let time = 0;  //触屏时间
let number;  
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showDialog: false,
    chooseCard:true
  },
  Goback: function () {
    wx.navigateBack({
      delta: 1
    })
  },
  applyPosition:function(){
    this.setData({
      showDialog:true
    })
    // wx.showToast({
    //   title: '尚未开放此功能',
    //   icon: 'none',
    //   duration: 2000,
    //   mask: true
    // })
  },
  toggleDialog: function () {
    this.setData({
      showDialog: !this.data.showDialog
    })
  },
  sureForposition:function(){
    // wx.redirectTo({
    //   url: './../cityLists/cityLists',
    // })
    wx.redirectTo({
      url: './../resume/resume',
    })
  },
  Goback:function(){
    wx.reLaunch({
      url: './../circle/circle',
    })
  },
  enterpriseMsg: function (vrTag) {
    let _that = this
    wx.request({
      url: common.formal + '/enterprise/getEnterpriseMsg.form',
      data: {
        vrTag: vrTag
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        _that.setData({
          enterpriseDetail:json.data.data
        })
      }
      })
      },
  positionInit: function (id) {
    let _that = this
    wx.request({
      url: common.formal + '/vrCard/selectPosition.form',
      data: {
       positionId:id
      },
      method: 'get',
      success: function (json) {
        let positionList = json.data.data
        if (positionList==undefined && positionList != '' && positionList != 'null') {
          _that.setData({
            positionList: json.data.data,
            positionName: json.data.data.positionName
          })
        } else {
          _that.setData({
            positionList: json.data.data,
            showpositionList: false,
            positionName: json.data.data.positionName
          })
        }

      }
    })
  },
  elseposition: function () {
    let that = this
    wx.request({
      url: common.formal + '/vrCard/selectPositionList.form',
      data: {
        start:0,
        end:5
      },
      method: 'get',
      success: function (json) {
        let elseposition = json.data.data
        that.setData({
          elseposition: elseposition
        })
      }
    })
  },
  topositionDetail: function (e) {
    wx.navigateTo({
      url: './../positionDetail/positionDetail?id=' + e.currentTarget.dataset.id + '&vrtag=' + e.currentTarget.dataset.vrtag,
    })
  },
  Goleft: function () {
    wx.switchTab({
      url: '../trans/trans'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var _that = this
    wx.getSystemInfo({
      success: function (res) {
        // 获取可使用窗口宽度
        let clientHeight = res.windowHeight;
        // 获取可使用窗口高度
        let clientWidth = res.windowWidth;
        // 算出高度(单位rpx)
        let height = clientHeight
        // 设置高度
        _that.setData({
          height: height
        })
      },
    });
    this.positionInit(options.id)
    this.enterpriseMsg(options.vrtag)
    this.elseposition()
    this.setData({
      id: options.id,
      vrtag: options.vrtag
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var _this = this;
    console.log(res.target, _this.data.id, _this.data.vrtag)
    if (res.from === 'button') {
    }
    return {
      title: '当前正在招聘'+_this.data.positionName +'职位',
      path: "pages/positionDetail/positionDetail?id=" + _this.data.id + '&vrTag=' + _this.data.vrtag + '&wherePage=' + 'positionDetail' + '&articleId=' + _this.data.id,
      imageUrl: ''
    }
  }
})