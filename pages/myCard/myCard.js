// pages/myCard/myCard.js
const common = require('../../js/config.js');
const jsonjs = require('../../js/json2.js');
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
      phone:"",
      userName: "",
      enterprise:"",
      position:"",
      visitedNum:"",  
      collectNum: "",   
      giveLikeNum: "",
      email:"暂未填写",
      imgurls:'',
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var _this=this;
      console.log(app.globalData.vrtag,options)
    _this.options = options;
    wx.request({
      url:  common.formal +'/card/getShareCardUrl.form',
      data: {
        objectId:options.userId
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        _this.setData({
          imgurls: json.data.data.imageUrl
        })
      },
    })
      wx.request({
      // url: common.domain + '/mini/getCardMsg.form',
      url:  common.formal +'/member/confine/getCardMsg.form',
      data: {
        test: options.test==undefined?common.test:options.test,
        vrTag: options.vrTag == undefined?common.vrTag : options.vrTag,
        userId: options.userId
      },
      method: 'get',
      success: function (json) {
        console.log(json);
        if (json.data.phone==null){
          json.data.phone="暂未填写"
        }
        if (json.data.position == null) {
          json.data.position = ""
        }
        _this.setData({
          enterpriseAddr: json.data.enterpriseAddr,
          enterprisePhone: json.data.enterprisePhone,
          enterpriseEmail: json.data.enterpriseEmail,
          phone: json.data.phone,
          userName: json.data.userName,
          enterprise: json.data.enterpriseName,
          position: json.data.position,
          visitedNum: json.data.visitedNum,
          collectNum: json.data.collectNum,
          giveLikeNum: json.data.givelikeNum
        })
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var _this=this;
      if(res.from==='button'){
      }
      return {
        title:"您好，这是我的名片，望惠存",
        path: "pages/TaCard/TaCard?objectId=" + _this.options.userId + '&vrTag=' + common.vrTag + '&imageUrl=' + _this.data.imgurls+'&toPage="tapage"',
        imageUrl: _this.data.imgurls
      }
  }
})