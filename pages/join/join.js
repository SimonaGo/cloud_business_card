// pages/join/join.js
const common = require('../../js/config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    word:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var _this=this;
      var scene=decodeURIComponent(options.scene);
      var scene='userId-123456,vrTag-0000';
      var sceneArr = scene.split(',');
      _this.para={};
    for (var i in sceneArr){
      var name = sceneArr[i].slice(0, sceneArr[i].indexOf('-'));
      var value = sceneArr[i].slice(sceneArr[i].indexOf('-')+1);
      _this.para[name] = value
    }
    console.log(sceneArr, _this.para);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  toJoin:function(){
    var _this=this;
    wx.request({
      url: common.domain + '/joinToEnterprise.form',
      data: _this.para,
      method: 'get',
      success: function (json) {
        if(json.data.status=='1'){
          _this.setData({
            word: "绑定成功"
          })
        }
        else{
          _this.setData({
            word: "绑定失败,请稍后再试"
          })
        }
      }
    });
  }
})