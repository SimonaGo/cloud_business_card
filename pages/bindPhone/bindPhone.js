// pages/bindPhone/bindPhone.js
const common = require('../../js/config.js');
var countdown = 60;
const app = getApp();
var settime = function (that) {
  if (countdown == 0) {
    that.setData({
      codeIsCanClick: true
    })
    countdown = 60;
    return;
  } else {
    that.setData({
      codeIsCanClick: false,
      last_time: countdown
    })
    countdown--;
  }
  setTimeout(function () {
    settime(that)
  }, 1000
  )
}
Page({
  data: {
    phoneNumber:'',
    securityNumbers:'',
    vrTag:'',
    unionid:'',
    codeIsCanClick: true,
    SureCanClicks: false
  },
  inputTyping: function (e) {
    this.setData({
      phoneNumber: e.detail.value
    });
  },
  inputTypings: function (e) {
    this.setData({
      securityNumbers: e.detail.value
    });
  },
  savePhone:function(e){
    console.log(e.detail.value)
    this.setData({
      phone:e.detail.value
    })
  },
  checkPhone: function(){ 
    var that = this
  var phone = that.data.phoneNumber
   if(!(/^1[3456789]\d{9}$/.test(phone))){ 
     wx.showToast({
       title: '请输入有效的手机号',
       icon:'none',
       duration: 2000
     })
       return false; 
   }else{
     that.securityNums()
   }
},
  securityNums: function (e) {
    var that = this;
    settime(that)
    var phone = this.data.phoneNumber
    if (that.data.phoneNumber!=''){
      wx.request({
        // url: common.domain+'/getCode.form',
        url: common.formal + '/user/getCode.form',
        data: {
          test: common.test,
          phone: this.data.phoneNumber
        },
        method: 'get',
        success: function (json) {
          console.log(json);
          that.setData({
            SureCanClicks: true
          })
        }
      });
    }else{
      wx.showToast({
        title: '请输入有效的手机号！',
      })
    }
   
  },
  sure: function(e){
    var that = this;
    var usernum ='';
    wx.request({
      // url: common.domain +'/userMember.form',
      url: common.formal + '/member/confine/userMember.form',//扫码绑定先绑定后生成名片
      data: {
        test: common.test,
        phone: this.data.phoneNumber,
        code: this.data.securityNumbers,
        vrTag: this.data.vrTag,
        unionid: this.data.unionid
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        if(json.data.status != '1'){
          wx.showToast({
            title: json.data.msg,
          })
        }else{
          usernum = json.data.data;
          console.log(usernum)
          wx.showToast({
            title: json.data.msg,
            duration: 2000
          })
          wx.request({
            // url: common.domain + '/shareMsg.form',
            url: common.formal + '/card/shareMsg.form',//生成名片分享图片
            data: {
              test: common.test,
              userId: usernum,
              unionid: that.data.unionid,
              vrTag:that.data.vrTag
            },
            method: 'get',
            success: function (data) {
              console.log(data)
            }
          })
          if (json.data.status == '1') {
            wx.reLaunch({
              url: '/pages/trans/trans?bindphone=1'
            })
          }
        }
      }
    })
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this =this;
    _this.options = options;
    wx.request({
      // url: common.domain + '/shareMsg.form',
      url: common.formal + '/member/createMember.form',//创建member信息
      data: {
        unionid: _this.options.unionid
      },
      method: 'get',
      success: function (data) {
        console.log(data)
      }
    })
    this.setData({
      vrTag: _this.options.vrTag,
      unionid:_this.options.unionid
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})