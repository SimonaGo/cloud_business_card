// pages/Myinfor/Myinfor.js
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
import CommonPage from "../CommonPage";
class IndexPage extends CommonPage {
  constructor(...args) {
    super(...args);
    this.data = {
      ready: false
    }
  }
  Gocontect = function () {
    // this.$route({ path: './../homepage/homepage', query: { count: 10, title: '这是第二个页面' }, clazzName: 'SecondPage' });
    wx.reLaunch({
      url: './../homepage/homepage',
    })
  }
  applycation= function (event) {
    console.log(event.target.id); 
    wx.navigateTo({
      url: './../updateVIP/updateVIP',
    })
    // wx.showToast({
    //   title: "暂未开放此模块",
    //   icon: "none",
    //   duration: 1000,
    //   mask: false,
    // })
  }
  ToVIP=function (event) {
    console.log(event.target.id); 
    var that = this
    wx.navigateTo({
      url: './../updateVIP/updateVIP?enterpriseId=' + this.data.enterpriseId + '&objectId=' + that.data.objectId1,
    })
  }
  GoAllmycard=function(){
    this.$route({ path: './../allmyCard/allmyCard', query: { count: 10, title: '这是第二个页面' }, clazzName: 'SecondPage' });
   
  }
  Goopinion=function(){
    wx.navigateTo({
      url: './../OpinionCollection/OpinionCollection',
    })
    // wx.showToast({
    //   title: "暂未开放此模块",
    //   icon: "none",
    //   duration: 1000,
    //   mask: false,
    // })
  }
  GoHide=function(){
    wx.navigateTo({
      url: './../CardTag/CardTag',
        })
  }
  Godiscount= function () {
    wx.navigateTo({
      url: './../discount/discount',
    })
    // wx.showToast({
    //   title: "暂未开放此模块",
    //   icon: "none",
    //   duration: 1000,
    //   mask: false,
    // })
  }
  taCardInit = function (unionid) {
    var _that = this
    wx.request({
      url: common.formal + '/transInit.form',
      data: {
        unionid: unionid,
        contactList: "false",
        start:0,
        end:5
      },
      method: 'get',
      success: function (json) {
        wx.hideLoading()
        _that.setData({
          ready:true
        })
        console.log(json)
          _that.setData({
            enterpriseId:json.data.data.objectEnterpriseId,
            objectId1: json.data.data.objectId
          })
        if (json.data.status == '3' || json.data.status == '5' || json.data.status == '6') {
          wx.redirectTo({
            url: '../base_card_addition/base_card_addition'
          })
          _that.setData({
            visitor: true
          })
        } else if (json.data.status != 0) {
          app.globalData.imageUrl = json.data.data.imageUrl,
            app.globalData.objectId = json.data.data.objectId,
            app.globalData.vrtag = json.data.data.vrTag
          if (json.data.data.contactList.length == '0' || json.data.data.contactList == null) {
            _that.setData({
              friendLists: true,
              "companyInfor[0]": json.data.data,
              vrTag: json.data.data.vrTag,
              objectId: json.data.data.objectId,
              imageUrl: json.data.data.imageUrl,
              hideLoadMore: false,
              showloadmore: true,
              alldata: 0,
              memberId: json.data.data.memberId,
              noContactlist: true,
              identityStatus: json.data.data.identityStatus,
              objectType: json.data.data.objectType,
              phone: json.data.data.phone
            })
          } else if (json.data.data.vrTag == null) {
            _that.setData({
              "companyInfor[0]": json.data.data,
              friendList: json.data.data.contactList,
              vrTag: json.data.data.vrTag,
              objectId: json.data.data.objectId,
              imageUrl: json.data.data.imageUrl,
              hideLoadMore: false,
              showloadmore: true,
              alldata: json.data.data.contactList.length,
              memberId: json.data.data.memberId,
              noContactlist: false,
              identityStatus: json.data.data.identityStatus,
              objectType: json.data.data.objectType,
              phone: json.data.data.phone
            })
          } else {
            _that.setData({
              "companyInfor[0]": json.data.data,
              friendList: json.data.data.contactList,
              vrTag: json.data.data.vrTag,
              objectId: json.data.data.objectId,
              imageUrl: json.data.data.imageUrl,
              hideLoadMore: false,
              showloadmore: true,
              alldata: json.data.data.contactList.length,
              memberId: json.data.data.memberId,
              identityStatus: json.data.data.identityStatus,
              objectType: json.data.data.objectType,
              phone: json.data.data.phone
            })
          }
        }
        
      },
       fail: function () {
        wx.hideLoading()
        wx.showModal({
          title: '请求超时',
          content: '请重启小程序或联系数据库管理员',
          success: function (res) {
            if (res.confirm) {
              wx.navigateBack({
                delta: -1
              })
            } else if (res.cancel) {
              wx.navigateBack({
                delta: -1
              })
            } else {
              wx.navigateBack({
                delta: -1
              })
            }
          }
        })
      }
    })
  }
  callCusPhone= function () {
    wx.makePhoneCall({
      phoneNumber: this.data.phone,
      success: function () {
        console.log("拨打电话成功！")
      },
      fail: function () {
        wx.showToast({
          title: "暂未填写电话",
          icon: "none",
          duration: 1000,
          mask: false,
        })
        console.log("拨打电话失败！")
      }
    })
  }
  toaddition=function () {
    if (this.data.identityStatus == '1' && this.data.objectType == '1') {
      // 企业基础版
      wx.showToast({
        title: "您不可修改企业名片",
        icon: "none",
        duration: 1000,
        mask: false,
      })
    } else if (this.data.identityStatus == '2' && this.data.objectType == '1') {
      // 企业升级版
      wx.showToast({
        title: "您不可修改企业名片",
        icon: "none",
        duration: 1000,
        mask: false,
      })
    }else{
      var that = this
      var array = this.data.companyInfor
      array[0].colleagueList = '';
      console.log(JSON.stringify(array))
      let datas = JSON.stringify(array)
      wx.navigateTo({
        url: '../base_card_addition/base_card_addition?data=' + datas + '&objectId=' + that.data.objectId + '&imageUrl=' + that.data.imageUrl
      })
    }
  }
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad= function (options) {
  
    let _that = this
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    if (app.globalData.unionid && app.globalData.unionid != '') {
      let unionid = app.globalData.unionid;
      console.log(unionid)
      _that.setData({
        unionid: unionid
      })
      this.taCardInit(unionid)
      console.log(unionid)
      console.log("unionid存在")
    } else {
      app.unionidCallback = unionid => {
        if (unionid != '') {
          _that.setData({
            unionid: unionid
          })
          // this.taCardInit(unionid)
          console.log(unionid)
        }
      }
    }
  }
  
  
  onShareAppMessage=function () {
   
    var _this = this;
    console.log(_this.data.imageUrl)
    return {
      title: "您好，这是我的名片，望惠存",
      path: "pages/TaCard/TaCard?objectId=" + _this.data.objectId + '&vrTag=' + _this.data.vrTag + '&imageUrl=' + _this.data.imageUrl + '&memberId=' + _this.data.memberId,
      imageUrl: _this.data.imageUrl
    }
  }
}
Page(new IndexPage())