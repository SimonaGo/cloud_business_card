/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-05 09:39:23
 * @LastEditTime: 2019-08-29 09:58:56
 * @LastEditors: Please set LastEditors
 */
// pages/carDetail/carDetail.js
const common = require('../../js/config.js');
const author = require('../../js/author.js');
const util = require('../../utils/util');
var app = getApp();
Page({
  data: {
    loadall:false,
    userid: '',
    vrTag: '',
    imageUrl: '',
    objectId: '',
    unionid:'',
    hideLoadMore: true,
    showloadmore: true,
    allfriend: '全部',
    condition: 15,
    alldata: 0,
    company:'',
    memberId:'',
    mark:false,
    h1:'',
    height:0,
    dats:'',
    erWeiMa2:'none',
    companyInfor: [
    ],
    friendList: [
    ],
    personal: false,
    src: '../../img/ps.png',
    img_url: '',
    img_background: 'http://a.wdsvip.com/vrCard/enterprise/wds.png',
    'censusList': [
      {
        type: "访客",
        number: "0",
        img_url:'../../img/yanjing1.png'
      },
      {
        type: "标星",
        number: 0,
        event:'addCardToBag',
        img_url:"../../img/xingx1.png"
      },
      {
        type: "点赞",
        number: "0",
        event: 'giveLikeToCard',
        img_url:"../../img/dianzan1.png"
      }
    ]
  },
  toaddition:function(){
    var that = this
    var array = this.data.companyInfor
    array[0].colleagueList='';
    console.log(JSON.stringify(array))
    let datas=JSON.stringify(array)
    wx.navigateTo({
      url: '../base_card_addition/base_card_addition?data='+datas+'&objectId='+that.data.objectId+'&imageUrl='+that.data.imageUrl
    })
  },
  Ercode:function(){
      this.setData({
        erWeiMa2:'block'
      })
  },
  closeModal:function(){
      this.setData({
        erWeiMa2:'none'
      })
  },
  tobill:function(){
    wx.navigateTo({
      url: '../canvas/canvas?objectId=' + this.data.objectId 
    })
  },
  tonewsList: function () {
    wx.navigateTo({
      url: '../newsList/newsList?objectId=' + this.data.objectId + 
        '&vrTag=' + this.data.vrTag +'&objectInfo='+ this.data.objectInfo +'&enterpriseName=' + this.data.enterpriseName
    })
  },
  selectAll: function () {
    if (this.data.condition == this.data.alldata) {
      wx.showToast({
        title: '已加载全部',
      })
    } else {
      this.setData({
        condition: this.data.alldata,
        showloadmore: false
      });
    }
  },
  selecthree: function () {
    this.setData({
      condition: 15,
      showloadmore: !this.data.showloadmore
    });
  },
  giveLikeToCard: function () {
    var _that = this;
    wx.request({
      // url: common.domain + '/mini/giveLikeToCard.form',
      url: common.formal + '/card/giveLikeToCard.form',
      // url: 'https://minsu.vryundian.com:6449/card/giveLikeToCard.form',
      data: {
        objectId: _that.data.objectId,
        unionid: _that.data.unionid,
        test: common.test
      },
      method: 'get',
      success: function (json) {
        console.log(json)
        if (json.data.status == '1') {
          _that.setData({
            'censusList[2].number': parseInt(_that.data.censusList[2].number) + 1
          })
        } else if (json.data.status == '2'){
          wx.showToast({
            title: '已点过赞',
          })
        }
      }
    })
  }, 
  addCardToBag: function (objectId, memberId, test) {
    var _that = this;
    wx.request({
      // url: common.domain + '/mini/addCardToBag.form',
      url: common.formal + '/card/addCardToBag.form',
      // url: 'https://minsu.vryundian.com:6449/card/addCardToBag.form',
      data: {
        objectId: _that.data.objectId,
        unionid: _that.data.unionid,
        test: common.test
      },
      method: 'get',
      success: function (json) {
        if (json.data.status == '1') {
          _that.setData({
            'censusList[1].number': parseInt(_that.data.censusList[1].number) + 1
          })
        } else if (json.data.status == '2') {
          wx.showToast({
            title: '您已收藏过',
          })
        }
      }
    })
  },
  moreMyCard: function () {
    wx.navigateTo({
      url: '../allmyCard/allmyCard'
    })
  },
  tolinkList: function () {
    wx.navigateTo({
      url: '../link/link'
    })
  },
  getSentenceinfor: function (objectId) {
    var _that = this
    wx.request({
      url: common.formal + '/selectSentence.form',
      method: 'get',
      success: function (json) {
        console.log(json)
        _that.setData({
          Sentenceinfor:json.data.data.sentenceContent
          })
        }
      })
    },
  getinfor: function (objectId,objectType) {
    var _that = this
    wx.request({
      url: common.formal + '/objectTransInit.form',
      data: {
        objectId: objectId,
        colleagueList:"true",
      },
      method: 'get',
      success: function (json) {
        _that.setData({
          loadall:true
        })
        if(objectType=='1'){
          console.log(json.data.data.objectInfo)
          _that.setData({
            companyOrpersonal:false,
            vrTag: json.data.data.vrTag,
            objectInfo: json.data.data.objectInfo,
            enterpriseName: json.data.data.enterpriseName
          })
        }
        console.log(json)
        if (json.data.data.visitedNum==null){
          json.data.data.visitedNum=0
        } if (json.data.data.collectNum == null) {
          json.data.data.collectNum = 0
        } if (json.data.data.givelikeNum== null) {
          json.data.data.givelikeNum = 0
        }
        console.log(json.data.data)
        // if (json.data.data.phone == null) {
        //   wx.redirectTo({
        //     url: '/pages/msg/msg',
        //   })
        // }
        console.log(json.data.data.colleagueList)
          _that.setData({
            "companyInfor[0]": json.data.data,
            friendList: json.data.data.colleagueList,
            vrTag: json.data.data.vrTag,
            objectId: json.data.data.objectId,
            imageUrl: json.data.data.imageUrl,
            hideLoadMore: false,
            showloadmore: true,
            alldata: json.data.data.colleagueList== null ? '0' : json.data.data.colleagueList.length,
            company: json.data.data.enterpriseName,
            'censusList[0].number': json.data.data.visitedNum,
            'censusList[1].number': json.data.data.collectNum,
            'censusList[2].number': json.data.data.givelikeNum,
            memberId: json.data.data.memberId,
            erWeiMa: json.data.data.cardCodeUrl,
            objectId:json.data.data.objectId,
            objectName:json.data.data.objectName,
            avatarUrl:json.data.data.avatarUrl
          })
        wx.hideLoading()
        var query = wx.createSelectorQuery();
        var h1 =0;
        query.select('.content1').boundingClientRect(function (res) {
          h1= parseInt(_that.data.height) - parseInt(res.height)
          _that.setData({
             h1:parseInt(_that.data.height)*2 - parseInt(res.height)*2-67
          })
        }).exec();
      },
      fail: function () {
        wx.showModal({
          title: '请求超时',
          content: '请重启小程序或联系数据库管理员',
          success: function (res) {
            if (res.confirm) {
            }
          }
        })
      }
    })
  },
  toVr: function () {
    console.log(this.data.unionid,this.data.vrTag,this.data.objectId,this.data.imageUrl,this.data.identityStatus,this.data.objectType)
    wx.navigateTo({
      url: '../index/index?unionid=' + this.data.unionid + "&vrTag=" + this.data.vrTag + "&meobjectId=" + this.data.objectId + "&imageUrl=" + this.data.imageUrl + "&identityStatus=" + this.data.identityStatus + "&objectType=" + this.data.objectType + "&objectId=" + this.data.objectId
    })
  },
  toSomeOne:function(e){
   console.log(e.currentTarget.dataset)
    var target = e.currentTarget.dataset
    wx.navigateTo({
      url: "../TaCard/TaCard?objectId=" + target.objectid + '&vrTag=' + target.vrtag + '&imageUrl=' + target.imageurl + '&memberId=' + target.memberid
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _that = this;
    if (app.globalData.unionid && app.globalData.unionid != '') {
      let unionid = app.globalData.unionid;
      console.log(unionid)
      _that.setData({
        unionid: unionid
      })
    } else {
      app.unionidCallback = unionid => {
        if (unionid != '') {
          _that.setData({
            unionid: unionid
          })
        }
      }
    }
   this.getSentenceinfor();
    console.log(options)
    this.setData({
      identityStatus:options.identityStatus,
      objectType:options.objectType
    })
    var times =1;
    wx.showLoading({
      title: '加载中',
      mask:true,
    })
    let time = util.formatTime(new Date());  
    let date = util.getDates(1,time)
    date=[...date]
    time = util.splitarr(time)
    console.log(date)
    _that.setData({  
      time: time,
      dats:date[0].week,
      month:date[0].month
    });  
    console.log(options)
    wx.getSystemInfo({
      success: function (res) {
        // 获取可使用窗口宽度
        let clientHeight = res.windowHeight;
        // 获取可使用窗口高度
        let clientWidth = res.windowWidth;
        // 算出高度(单位rpx)
        let height = clientHeight
        // 设置高度
        _that.setData({
          height: height
        })
        console.log(_that.data.height)
      },
    });
    console.log(options)
      var _that = this
    if (options.objectId){
      _that.setData({
        objectId:options.objectId
        })
      _that.getinfor(options.objectId,options.objectType);
    }
    // 类型判断开始
        if(options.identityStatus=='1'&&options.objectType=='1'){
            // 企业基础版
            _that.getSentenceinfor();
            _that.setData({
              freeForCompany:true,
              colleageList:true,
              onlyPersonal:false
            })
        }else if(options.identityStatus=='2'&&options.objectType=='1'){
          // 企业升级版
            _that.getSentenceinfor();
            _that.setData({
              updateForCompany:true,
              colleageList:true,
              onlyPersonal:false
            })
        }else if(options.identityStatus=='1'&&options.objectType=='2'){
              // 个人基础版
              _that.getSentenceinfor();
              _that.setData({
                colleageList:false,
                onlyPersonal:true,
                personal:true
              })
        }else if(options.identityStatus=='2'&&options.objectType=='2'){
          // 个人升级版
          _that.getSentenceinfor();
          _that.setData({
            colleageList:false,
            onlyPersonal:true,
            personal: true
          })
        }
        // 类型判断结束
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.addCardToBag()
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var _this = this;
    console.log('sahre');
    if (res.from === 'button') {
    }
    return {
      title: "您好，这是我的名片，望惠存",
      path: "pages/TaCard/TaCard?objectId=" + _this.data.objectId + '&vrTag=' + _this.data.vrTag + '&imageUrl=' + _this.data.imageUrl + '&memberId=' + _this.data.memberId,
      imageUrl: _this.data.imageUrl
    }
  }
})