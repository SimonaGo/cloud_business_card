// pages/allMyCard/allMyCard.js
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
Page({
  data: {
    userid: '',
    vrTag: '',
    unionid: '',
    length:true,
    'companyInfor': [
    ],
    src: '../../img/ps.png',
    'companyInfors': [
    ],
    'friendList': [ ],
    img_url: "",
    Msg:''
  },
  taCardInit: function (unionid) {
    var _that = this
    wx.request({
      url: common.formal + '/card/getcardList.form',
      data: {
        unionid: unionid
      },
      method: 'get',
      success: function (json) {
        console.info(json)
        if (json.data.status == 0) {
          wx.redirectTo({
            url: '/pages/msg/msg',
          })
        }else if(json.data.status=='3'){
          _that.setData({
            Msg:json.data.msg
          })
        }
        else{
          if (_that.data.friendList.concat(json.data.data).length == 0) {
            _that.setData({
              length: false
            })
          }
          _that.setData({
            "companyInfor[0]": json.data.data,
            friendList: _that.data.friendList.concat(json.data.data),
          })
        }   
        wx.hideLoading()     
      },
      fail: function () {
        wx.showModal({
          title: '请求超时',
          content: '请重启小程序或联系数据库管理员',
          success: function (res) {
            if (res.confirm) {
              console.log('确定')
            }
          }
        })
      }
    })
  },
  tovr: function (e) {
    var thats =e.currentTarget.dataset
    console.log(thats)
    wx.navigateTo({
      url: '../TaCard/TaCard?objectId=' + thats.objectid + "&vrTag=" + thats.vrtag+ "&memberId=" + thats.memberid  + '&imageUrl=' + thats.imageurl,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (option) {
    wx.showLoading({
      title: '加载中',
      mask: true,
    })
    var _that = this
    console.log(1)
    if (app.globalData.unionid == '') {
      console.log(2)
      app.unionidCallback = unionid => {
        _that.setData({
          unionid: unionid
        })
        this.taCardInit(unionid);
      }
    } else {
      console.log(3)
      var unionid = app.globalData.unionid
      _that.setData({
        unionid: unionid
      })
      this.taCardInit(unionid);
    }
  },
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    var _this = this;
    if (res.from === 'button') {
    }
    return {
      title: "您好，这是我的名片，望惠存",
      path: "pages/TaCard/TaCard?objectId=" + _this.data.objectId + '&vrTag=' + _this.data.vrTag + '&imageUrl=' + _this.data.imageUrl,
      imageUrl: _this.data.imageUrl
    }
  }
})