// pages/recruitCenter/recruitCenter.js
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showdiff:true,
    showdiff1:false,
    position:true,
    forPosition:false,
    upordown:0
  },
  Goback: function () {
    console.log('er')
    wx.navigateBack({
      delta: 1
    })
  },
  tocityLists:function(){
    this.setData({
      upordown:1
    })
    wx.navigateTo({
      url: './../cityLists/cityLists',
    })
  },
  hideInput:function () {
    this.setData({
      inputVal: "",
      inputShowed: false
    });
  },
    clearInput:function () {
    this.setData({
      inputVal: ""
    });
  },
    inputTyping: function (e) {
    this.setData({
      inputVal: e.detail.value
    });
  },
  showInput: function () {
    this.setData({
      inputShowed: true
    });
  },
  changeStatus:function(){
    if (this.data.showdiff==true){
      this.setData({
        showdiff: true
      })
    }else{
      this.setData({
        showdiff: !this.data.showdiff,
        showdiff1: false,
      })
    }
  },
  changeStatus1: function () {
    if (this.data.showdiff1 == true) {
      this.setData({
        showdiff1: true
      })
    }else{
      this.setData({
        showdiff1: !this.data.showdiff1,
        showdiff: false
      })
    }
   
  },
  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target,
      upordown1: 1
    })
  },
  showModal1(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target,
      upordown2: 1
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
    this.setData({
      upordown1: 0
    })
    this.setData({
      upordown2: 0
    })
  },
  tosearch1(e){
    console.log('点击了搜索条件')
    console.log(e.currentTarget.dataset.condition)
    this.setData({
      upordown1: 0
    })
    this.setData({
      modalName: null
    })
  },

  tosearch2(e) {
    console.log('点击了搜索条件')
    console.log(e.currentTarget.dataset.condition)
    this.setData({
      upordown2: 0
    })
    this.setData({
      modalName: null
    })
  },
  toposition(e){
    wx.navigateTo({
      url: './../positionDetail/positionDetail?id='+e.currentTarget.dataset.id + '&vrtag=' +e.currentTarget.dataset.vrtag,
    })
  },
  positionInit: function (vrTag) {
    var _that = this
    wx.request({
      url: common.formal + '/vrCard/selectPositionList.form',
      data: {
        start: 0,
        end: 10
      },
      method: 'get',
      success: function (json) {
        console.log(json.data.data)
        // json.data.data.forEach(x => {
        //   x.ctime=x.ctime.substring(0,10)
        // });
        var positionList = json.data.data
        if (positionList.length != '0' && positionList != '' && positionList != 'null') {
          _that.setData({
            positionList: json.data.data,
            showpositionList: true,
            alldata: json.data.data.length
          })
        } else {
          _that.setData({
            positionList: json.data.data,
            showpositionList: false
          })
        }

      }
    })
  
  },
 seeker(){
   var that = this
   wx.request({
     url: 'http://rap2api.taobao.org/app/mock/234546/jobSeeker',
     data:{seeker:''},
     method:'get',
     success:function(json){
       console.log(json.data.seekerStatus)
        that.setData({
          seekerinfor:json.data.seekerInfor,
          seekerStatus: json.data.seekerStatus[0]
        })
     }
   })
 },
  checkCard(){
  wx.showToast({
    title: '该功能暂未开放',
    icon: "none",
    duration: 1000,
    mask: false
  })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.positionInit()
    this.seeker()
    this.setData({
      upordown: 0
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})