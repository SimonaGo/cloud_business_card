// pages/collectionDetail/collectionDetail.js
import WxValidate from '../../utils/WxValidate';
const common = require('../../js/config.js');
const App = getApp()
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    min: 5,//最少字数
    max: 200, //最多字数 (根据自己需求改变)
    files: [],
    showTopTips: false,
    fromChange: 1,
    pathUrl: '',
    subdisabled:false,
    showDialog: false
  },
  goback:function(){
    //返回上一级关闭当前页面
    wx.navigateBack({
      delta: 1
    })
  },
  toggleDialog:function() {
    this.setData({
      showDialog: !this.data.showDialog
    })
  },
  inputs: function (e) {
    // 获取输入框的内容
    var value = e.detail.value;
    // 获取输入框内容的长度
    var len = parseInt(value.length);

    //最少字数限制
    if (len <= this.data.min)
      this.setData({
        texts: "最低五个字"
      })
    else if (len > this.data.min)
      this.setData({
        texts: " "
      })

    //最多字数限制
    if (len > this.data.max) return;
    // 当输入框内容的长度大于最大长度限制（max)时，终止setData()的执行
    this.setData({
      currentWordNumber: len //当前字数
    });
  },
  submitForm(e) {
    var that = this
    const params = e.detail.value
    if (params.sentence==''){
      that.toggleDialog()
    }else{

    
    console.log(params.sentence)
    console.log(that.data.pathUrl)
      // wx.request({
      //   url: common.formal + '/addBaseCard.form',
      //   data: {
      //     baseName: e.detail.value.baseName,
      //     phone: e.detail.value.phone,
      //     mechanism: e.detail.value.mechanism,
      //     position: e.detail.value.position,
      //     code: e.detail.value.code,
      //     unionid: that.data.unionid,
      //     baseId: that.data.pid,
      //     avatarUrl: that.data.pathUrl
      //   },
      //   method: 'get',
      //   success: function (requestRes) {
      //     console.log(requestRes)
      //     if (requestRes.data.status=='2') {
      //       wx.showToast({
      //         title: requestRes.data.msg,
      //         icon:'none',
      //         during:2000
      //       })
      //       that.setData({
      //         subdisabled: false
      //       })
      //   }else{
      //     wx.reLaunch({
      //       url: '/pages/trans/trans'
      //     })
      //   }
      //   },
      //   fail: function () {
      //   },
      //   complete: function () {
      //   }
      // })
   
    }
  },
  chooseImage: function (e) {
    let _this = this
    _this.setData({
      subdisabled: true
    })//取json中的值
    if (_this.data.fromChange == 0) {
      console.log('请添加名片')
    } else {
      var that = this;
      wx.chooseImage({
        count: 5,
        sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
        sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
        success: function (res) {
          // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
          that.setData({
            files: that.data.files.concat(res.tempFilePaths)
          });
          const tempFilePaths = res.tempFilePaths
          wx.uploadFile({
            url: common.formal + '/report/uploadReport.form',
            filePath: tempFilePaths[0],
            name: 'reportFile',
            header: { 'content-type': 'multipart/form-data' },
            method: 'POST',
            formData: {
              'masterCatalog': 'vrCard',
              'pId': that.data.objectId,
              'delImgurls': that.data.pathUrl,
              'fileTag': 'cd-wh',
              'weight': 300,
              'height': 300,
              'fileType': '1-2'
            },
            success(res) {
              console.log(res.data)
              var strings = res.data
              var jsonObj = JSON.parse(res.data);//转换为json对象
              for (var i = 0; i < jsonObj.length; i++) {
                console.log(jsonObj[i].pathUrl);
                _this.setData({
                  pathUrl: jsonObj[i].pathUrl,
                  subdisabled:false
                })//取json中的值
              }
              const data = res.data
              //do something
            }
          })
        }
      })

    }
  },
  previewImage: function (e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: this.data.files // 需要预览的图片http链接列表
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    let _that = this
    _that.setData({
      titles: options.flag
    })
    wx.getSystemInfo({
      success: function (res) {
        // 获取可使用窗口宽度
        let clientHeight = res.windowHeight;
        // 获取可使用窗口高度
        let clientWidth = res.windowWidth;
        // 算出高度(单位rpx)
        let height = clientHeight
        // 设置高度
        _that.setData({
          height: height
        })
        console.log(_that.data.height)
      },
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})