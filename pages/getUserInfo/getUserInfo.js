// pages/test/test.js
var app = getApp();
var optionsText='';
const common = require('../../js/config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this=this;
  },
  bindGetUserInfo: function (e) {
    var _this = this;
    console.log(_this.options);
    console.log(decodeURIComponent(_this.options.sessionKey));
    console.log(_this.options.unionid);
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserInfo({
            withCredentials: true,
            lang: "zh_CN",
            success: userinfo => {
                //授权成功
                wx.request({
                  // url: common.domain + "/saveUserInfo.form",
                  url: common.formal+'/member/confine/saveUserInfo.form',
                  data: {
                    // test: common.test,
                    vrTag: _this.options.scene,
                    nickName: userinfo.userInfo.nickName,
                    avatarUrl: userinfo.userInfo.avatarUrl,
                    gender: userinfo.userInfo.gender,
                    city: userinfo.userInfo.city,
                    province: userinfo.userInfo.province,
                    country: userinfo.userInfo.country,
                    language: userinfo.userInfo.language,
                    iv: userinfo.iv,
                    encryptedData: userinfo.encryptedData,
                    discountTypes: "0",
                    sessionKey: decodeURIComponent(_this.options.sessionKey),
                    unionid: _this.options.unionid
                  },
                  success: function (saveUserInfo) {
                    console.log(saveUserInfo)
                    if (saveUserInfo.data.unionid != undefined 
                      && saveUserInfo.data.unionid !=null
                      && saveUserInfo.data.unionid != "") {
                      app.globalData.unionid = saveUserInfo.data.unionid;
                      console.log(app.globalData.unionid)
                      if (app.unionidCallback) {
                        app.unionidCallback(saveUserInfo.data.unionid);
                      }
                    }
                   var  options = _this.options;
                    if (options.scene) {
                      console.log('来了老弟')
                      if(options.scene.indexOf('cardNo')!=-1){
                        console.log('老弟你好')
                       var num = options.scene.replace('cardNo','')
                        console.log(num)
                      var redirectToUrl = "/pages/TaCard/TaCard?cardNo="+num;
                     }else{
                      var redirectToUrl = "/pages/index/index?scene=" + options.scene;
                      console.log('index')
                     }
                    } else if (options.vrTag) {
                      var redirectToUrl = "/pages/TaCard/TaCard?objectId=" + options.objectId + '&vrTag=' + options.vrTag + '&imageUrl=' + options.imageUrl + '&memberId=' + options.memberId
                    } else {
                      redirectToUrl = "/pages/trans/trans";
                    }
                    wx.reLaunch({
                      url: redirectToUrl
                    });
                  }
                })
              
            },
            fail: function () {    //授权失败
            },
          });                 
        }
        else{
            wx.navigateBack({
              delta: 1
            });
        }
      }
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onGotUserInfo: function (e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.userInfo)
    console.log(e.detail.rawData)
    wx.request({
      url: "https://m.wdsvip.com/member/confine/saveUserUnionid.form",
      data: {
        // test:common.test,
        nickName: e.detail.userInfo.nickName,
        avatarUrl: e.detail.userInfo.avatarUrl,
        gender: e.detail.userInfo.gender,
        city: e.detail.userInfo.city,
        province: e.detail.userInfo.province,
        country: e.detail.userInfo.country,
        language: e.detail.userInfo.language
      },
      success: function (result) {
        console.log(result);
        _this.setData({
          hasInfo: "true",
          url: 'https://m.wdsvip.com/static/weicity/personal.html?proTag=miniwx'
        });
      }
    });
  }
})