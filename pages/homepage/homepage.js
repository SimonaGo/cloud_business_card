//index.js
//获取应用实例
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
import CommonPage from "../CommonPage";
class SecondPage extends CommonPage {
  constructor(...args) {
    super(...args);
      this.data={
        friendLists: false,
        showOrmiss: false,
        userid: '',
        vrTag: '',
        imageUrl: '',
        objectId: '',
        times: 0,
        hideLoadMore: true,
        showloadmore: true,
        allfriend: '全部',
        condition: 0,
        alldata: 0,
        memberId: '',
        unionid: '',
        companyInfor: '',
        binds: 0,
        height: 0,
        h1: '',
        here: '查看全部',
        imagemore: "../../img/gengduo.png",
        friendList: [],
        src: '../../img/ps.png',
        img_url: '',
        noContactlist: false,
        visitor: false
      }
  }
    showInput= function () {
      this.setData({
        inputShowed: true
      });
    }
      Goleft=function () {
      wx.switchTab({
        url: '../trans/trans'
      })
    }
    allTaCard= function () {
      console.log(this.data.showOrmiss)
      if (this.data.showOrmiss) {
        this.setData({
          condition: 3,
          showOrmiss: !(this.data.showOrmiss),
          here: "查看全部"
        });
        console.log(this.data.condition, this.data.here)
      } else {
        console.log(123)
        this.setData({
          condition: this.data.alldata,
          showOrmiss: !(this.data.showOrmiss),
          here: "收起全部"

        });
        console.log(this.data.condition, this.data.here)
      }
      console.log(this.data.showOrmiss, this.data.here)
    }
    hideInput=function () {
      this.setData({
        inputVal: "",
        inputShowed: false
      });
    }
    clearInput= function () {
      this.setData({
        inputVal: ""
      });
    }
    inputTyping= function (e) {
      this.setData({
        inputVal: e.detail.value
      });
    }
    selectAll= function () {
      if (this.data.condition >= this.data.alldata) {
        if (this.data.alldata == 0) {
          wx.showToast({
            title: '暂无联系人',
          })
        } else {
          wx.showToast({
            title: '已加载全部',
          })
        }
      } else {
        this.setData({
          condition: this.data.alldata,
          showloadmore: false
        });
      }
    }
    selecthree= function () {
      this.setData({
        condition: 3,
        showloadmore: true
      });
    }
    moreMyCard= function () {
      wx.navigateTo({
        url: '../allmyCard/allmyCard'
      })
    }
    toTaCard=function (e) {
      console.log(e.currentTarget.dataset, this.data.memberId)
      var that = e.currentTarget.dataset
      wx.navigateTo({
        url: '../TaCard/TaCard?objectId=' + that.objectid + '&vrTag=' + that.vrtag + '&imageUrl=' + that.imageurl + '&memberId=' + this.data.memberId
      })
    }
    toSomeOne=function (e) {
      console.log(e.currentTarget.dataset, this.data.memberId)
      var that = e.currentTarget.dataset
      wx.navigateTo({
        url: '../carDetail/carDetail?objectId=' + that.objectid + '&vrTag=' + that.vrtag + '&imageUrl=' + that.imageurl + '&memberId=' + this.data.memberId + '&unionid=' + this.data.unionid + '&identityStatus=' + this.data.identityStatus + '&objectType=' + this.data.objectType
      })
    }
    toaddition=function () {
      wx.reLaunch({
        url: '/pages/base_card_addition/base_card_addition'
      })
    }
    selectAvatarUrl= function (e) {
      var _this = this
      wx.request({
        url: common.formal + '/member/selectAvatarUrl.form',
        data: { unionid: e },
        method: 'get',
        success: function (json) {
          console.log(json.data.data)
          _this.setData({
            avatarUrl: json.data.data
          })
        }

      })
    }
    taCardInit= function (unionid) {
      var _that = this
      wx.request({
        url: common.formal + '/transInit.form',
        data: {
          unionid: unionid,
          contactList: "true",
          start: 0,
          end: 10
        },
        method: 'get',
        success: function (json) {
          if (json.data.status == '3' || json.data.status == '5') {
            wx.redirectTo({
              url: '../base_card_addition/base_card_addition'
            })
            _that.setData({
              visitor: true
            })
          }
          console.log(json)
          if (json.data.status == '6') {
            wx.reLaunch({
              url: '/pages/index/index?vrTag=c0001&unionid=' + unionid + '&objectId=EV8IE1K3H4PH1905YEC3&imageUrl=http://a.wdsvip.com/vrCard/enterprise/shareUser/EV8IE1K3H4PH1905YEC3/EV8IE1K3H4PH1905YEC3.png'
            })
          }
          else {
            console.log(json)
            // wx.reLaunch({
            //   url: '../carDetail/carDetail?objectId=' + json.data.data.objectId + 
            //     '&vrTag=' + json.data.data.vrTag + 
            //     '&imageUrl=' + json.data.data.imageUrl + 
            //     '&memberId=' + json.data.data.memberId + 
            //     '&unionid=' + json.data.data.unionid + 
            //     '&identityStatus=' + json.data.data.identityStatus + 
            //     '&objectType=' + json.data.data.objectType
            // })
            if (json.data.status != 0) {
              app.globalData.imageUrl = json.data.data.imageUrl,
                app.globalData.objectId = json.data.data.objectId,
                app.globalData.vrtag = json.data.data.vrTag
              if (json.data.data.contactList.length == '0' || json.data.data.contactList == null) {
                _that.setData({
                  friendLists: true,
                  "companyInfor[0]": json.data.data,
                  vrTag: json.data.data.vrTag,
                  objectId: json.data.data.objectId,
                  imageUrl: json.data.data.imageUrl,
                  hideLoadMore: false,
                  showloadmore: true,
                  alldata: 0,
                  memberId: json.data.data.memberId,
                  noContactlist: true,
                  identityStatus: json.data.data.identityStatus,
                  objectType: json.data.data.objectType
                })
                console.log(_that.data.friendList)
              } else if (json.data.data.vrTag == null) {
                _that.setData({
                  "companyInfor[0]": json.data.data,
                  friendList: json.data.data.contactList,
                  vrTag: json.data.data.vrTag,
                  objectId: json.data.data.objectId,
                  imageUrl: json.data.data.imageUrl,
                  hideLoadMore: false,
                  showloadmore: true,
                  alldata: json.data.data.contactList.length,
                  memberId: json.data.data.memberId,
                  noContactlist: false,
                  identityStatus: json.data.data.identityStatus,
                  objectType: json.data.data.objectType
                })
              } else {
                let arry = new Set();
                arry.add(_that.data.companyInfor[0])
                _that.setData({
                  "companyInfor[0]": json.data.data,
                  friendList: json.data.data.contactList,
                  vrTag: json.data.data.vrTag,
                  objectId: json.data.data.objectId,
                  imageUrl: json.data.data.imageUrl,
                  hideLoadMore: false,
                  showloadmore: true,
                  alldata: json.data.data.contactList.length,
                  memberId: json.data.data.memberId,
                  identityStatus: json.data.data.identityStatus,
                  objectType: json.data.data.objectType
                })
              }
            }
          }
          var query = wx.createSelectorQuery();
          //选择id
          var h1 = 0;
          query.select('.content1').boundingClientRect(function (res) {
            h1 = parseInt(_that.data.height) - parseInt(res.height)
            _that.setData({
              h1: parseInt(_that.data.height) * 2 - parseInt(res.height) * 2 - 67
            })
          }).exec();
          wx.hideLoading()
        },
        fail: function () {
          wx.hideLoading()
          wx.showModal({
            title: '请求超时',
            content: '请重启小程序或联系数据库管理员',
            success: function (res) {
              if (res.confirm) {
                wx.navigateBack({
                  delta: -1
                })
              } else if (res.cancel) {
                wx.navigateBack({
                  delta: -1
                })
              } else {
                wx.navigateBack({
                  delta: -1
                })
              }
            }
          })
        }
      })
    }
    onLoad=function (options) {
      wx.showLoading({
        title: '加载中',
        mask: true,
      })
      var _that = this
      _that.setData({
        condition: 5,
        times: _that.data.times++
      })
      wx.getSystemInfo({
        success: function (res) {
          // 获取可使用窗口宽度
          let clientHeight = res.windowHeight;
          // 获取可使用窗口高度
          let clientWidth = res.windowWidth;

          // 算出高度(单位rpx)
          let height = clientHeight
          // 设置高度
          _that.setData({
            height: height
          })
          console.log(_that.data.height)
        },
      });
      console.log(options)
      if (options != undefined && options.bindphone) {
        _that.setData({
          binds: 1
        })
      }
      if (app.globalData.unionid && app.globalData.unionid != '') {
        let unionid = app.globalData.unionid;
        console.log(unionid)
        _that.setData({
          unionid: unionid
        })
        this.taCardInit(unionid)
        console.log(unionid)
        console.log("unionid存在")
      } else {
        app.unionidCallback = unionid => {
          if (unionid != '') {
            _that.setData({
              unionid: unionid
            })
            // this.taCardInit(unionid)
            console.log(unionid)
          }
        }
      }
    }
    onShow=function () {
      //  this.setData({
      //   noContactlist:false
      //  })  
      //this.onLoad()
      //  this.taCardInit(this.data.unionid);
      //  if (this.data.binds == '1') {
      //    this.onLoad('2')
      //  }
    }
    onShareAppMessage= function (res) {
      var _this = this;

      if (res.from === 'button') {
      }
      return {
        title: "您好，这是我的名片，望惠存",
        path: "pages/TaCard/TaCard?objectId=" + _this.data.objectId + '&vrTag=' + _this.data.vrTag + '&imageUrl=' + _this.data.imageUrl + '&memberId=' + _this.data.memberId,
        imageUrl: _this.data.imageUrl
      }
    }
  }

Page(new SecondPage({ clazzName: 'SecondPage' }))
