// pages/allMyCard/allMyCard.js
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
import CommonPage from "../CommonPage";
class homePage extends CommonPage {
  constructor(...args) {
    super(...args);
    this.data = {
      userid: '',
      vrTag: '',
      addimg_url: '',
      img_url: "",
      deleteOrbind: "",
      addition: false,
      src: '../../img/ps.png',
      isMasterCard: false
    }
  }
 
toaddition= function () {
  wx.navigateTo({
    url: '/pages/base_card_addition/base_card_addition'
  })
}
setMaster=function (e) {
  var that = this
  console.log(e.target.dataset.objectid)
  wx.showModal({
    title: '将要执行更换操作',
    content: '确定要更换主名片吗？',
    success: function (sm) {
      if (sm.confirm) {
        // 用户点击了确定 可以调用删除方法了
        wx.request({
          url: common.formal + '/updateMasterCard.form',
          data: {
            oldMasterCardId: that.data.objectId,
            newMasterCardId: e.target.dataset.objectid
          },
          method: 'get',
          success: function (json) {
            if (getCurrentPages().length != 0) {
              //刷新当前页面的数据
              getCurrentPages()[getCurrentPages().length - 1].onShow()
            }
            // this.onShow()
          }
        })
      } else if (sm.cancel) {
        console.log('用户点击取消')
      }
    }
  })

}
deleteBaseCard=function(e) {
  console.log(e.target.dataset)
  wx.showModal({
    title: '您将要删除名片',
    content: '确定要删除吗？',
    success: function (sm) {
      if (sm.confirm) {
        // 用户点击了确定 可以调用删除方法了
        wx.request({
          url: common.formal + '/deleteBaseCard.form',
          data: {
            baseId: e.target.dataset.name,
          },
          method: 'get',
          success: function (json) {
            console.log(json)
            if (getCurrentPages().length != 0) {
              //刷新当前页面的数据
              getCurrentPages()[getCurrentPages().length - 1].onShow()
            }
          }
        })
      } else if (sm.cancel) {
        console.log('用户点击取消')
      }
    }
  })

}
deleteObjectMember= function() {
  console.log('点击了删除企业按键')
}
toSomeOne= function (e) {
  console.log(e.currentTarget.dataset)
  var that = e.currentTarget.dataset
  console.log(that, this.data.unionid)
  wx.navigateTo({
    url: '../carDetail/carDetail?objectId=' + that.objectid +
      '&imageUrl=' + that.imageurl +
      '&identityStatus=' + that.identitystatus +
      '&objectType=' + that.objecttype +
      '&memberId=' + that.memberid +
      '&vrTag=' + that.vrtag +
      '&unionid=' + this.data.unionid
  })
}
taCardInit= function (unionid) {
  var _that = this
  wx.request({
    url: common.formal + '/selectBaseCard.form',
    data: {
      unionid: unionid,
    },
    method: 'get',
    success: function (json) {
      console.log(json.data.data)
      let array = new Set();
      json.data.data.forEach(x => array.add(x))
      console.log(array.size)
      array.forEach(c => {
        console.log(parseInt(c.objectType))
        if (parseInt(c.masterSlaveState) === 1) {
          _that.setData({
            objectId: c.objectId,
            masterOfTaCard: json.data.data,
            addition: true,
            identityStatus: json.data.data.identityStatus,
            objectType: json.data.data.objectType,
            isMasterCard: false,
            memberId: json.data.data.memberId
          })
        }
        else {
          _that.setData({
            masterOfTaCard: json.data.data,
            addition: true,
            identityStatus: json.data.data.identityStatus,
            objectType: json.data.data.objectType
          })
        }
      })
      wx.hideLoading()
    },
    fail: function () {
      wx.hideLoading()
      wx.showModal({
        title: '请求超时',
        content: '请重启小程序或联系数据库管理员',
        success: function (res) {
          if (res.confirm) {
          }
        }
      })
    }
  })
}
onLoad=function (options) {
  var _that = this
  wx.showLoading({
    title: '加载中',
    mask: true,
  })
  if (app.globalData.unionid && app.globalData.unionid != '') {
    let unionid = app.globalData.unionid;
    console.log(unionid)
    _that.setData({
      unionid: unionid
    })
    this.taCardInit(unionid);
  } else {
    app.unionidCallback = unionid => {
      if (unionid != '') {
        _that.setData({
          unionid: unionid
        })
        this.taCardInit(unionid);
      }
    }
  }
}
onReady= function () {
}

/**
 * 生命周期函数--监听页面显示
 */
onShow=function () {
  this.taCardInit(this.data.unionid);
}
/**
 * 生命周期函数--监听页面隐藏
 */
onHide= function () {

}

/**
 * 生命周期函数--监听页面卸载
 */
onUnload= function () {

}

/**
 * 页面相关事件处理函数--监听用户下拉动作
 */
onPullDownRefresh=function () {

}

/**
 * 页面上拉触底事件的处理函数
 */
onReachBottom=function () {

}

/**
 * 用户点击右上角分享
 */
onShareAppMessage=function (res) {
  console.log(res.target.dataset.imageurl)
  console.log(res.target.dataset.objectid)
  console.log(res.target.dataset.memberid)
  console.log(res.target.dataset.vrtag)
  var _this = this;
  var imageUrls = res.target.dataset.imageurl
  var objectId = res.target.dataset.objectid
  var memberId = res.target.dataset.memberid
  var vrtag = res.target.dataset.vrtag
  console.log('123')
  if (res.from === 'button') {
  }
  return {
    title: "您好，这是我的名片，望惠存",
    path: "pages/TaCard/TaCard?objectId=" + objectId + '&vrTag=' + vrtag + '&imageUrl=' + imageUrls + '&memberId=' + memberId,
    imageUrl: imageUrls
  }
}
}
Page(new homePage())