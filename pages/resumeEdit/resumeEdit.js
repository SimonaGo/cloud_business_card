//index.js
//获取应用实例
var tcity = require("../../utils/city.js");
const util = require('../../utils/util');

var app = getApp()
Page({
  data: {
    condition: false,
    condition1: false,
    condition2: false,
    currentDate:'',
    currentPrice:'',
    expectedSalary:'',
    minDate: new Date().getTime(),
    maxDate: new Date().setDate(new Date().getDate()+7),
    columns1: ['5k以下','6k','7k','8k','9k','10k','11k','12k','13k','13k以上'],
    columns2: ['5k以下', '6k', '7k', '8k', '9k', '10k', '11k', '12k', '13k', '13k以上']
  },
    onInput(event) {
      console.log(event.detail)
      this.setData({
        currentDate: util.formatTimeTwo(event.detail)
      });
  },
  onconfirm(event) {
    this.setData({
      condition: false
    })
  },
  
  onChange1(event) {
    const { picker, value, index } = event.detail;
    this.setData({
      currentPrice: `${ value }`,
    })
  },
  onChange2(event) {
    const { picker, value, index } = event.detail;
    this.setData({
      expectedSalary: `${value}`
    })
  },
  oncancel(){
    this.setData({
      condition: !this.data.condition
    })
  },
  open: function () {
    
    this.setData({
      condition: !this.data.condition,
      condition1: false,
      condition2: false,
    })

  },
  open_van1:function(){
    this.setData({
      condition1: !this.data.condition1,
      condition: false,
      condition2: false
    })
  },
  open_van2: function () {
    this.setData({
      condition2: !this.data.condition2,
      condition: false,
      condition1: false
    })
  },
  onLoad: function () {
  }
})
