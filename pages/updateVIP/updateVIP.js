// pages/updateVIP/updateVIP.js
const common = require('../../js/config.js');
const author = require('../../js/author.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    stander: 'stander',
    deluxe: '',
    flagship: '',
    deluxecheck:false,
    flagshipchecks: false,
    checks:true,
    words:'申请升级',
    disabled:false
  },
  Goback:function(){
    wx.navigateBack({
      delta: 1
    })
  },
  checkvision:function(enterpriseId){
    var that = this
    wx.request({
      // url: common.domain + '/mini/giveLikeToCard.form',
      url: common.formal + '/selectVipStatus.form',
      // url: 'https://minsu.vryundian.com:6449/card/giveLikeToCard.form',
      data: {
        enterpriseId: enterpriseId
      },
      method: 'get',
      success: function (json) {
         console.log(json)
        if (json.data.data.enterpriseVipId != null && json.data.data.enterpriseVipId != undefined && json.data.data.enterpriseVipId !=''){
          that.setData({
            disabled:true,
            words:'正在升级...'
          })
        }
      }
    })
  },
  checkboxChange:function (e) {
    let that = this
    if (e.detail.value==''){
      this.setData({
        stander:'',
        deluxe: '',
        flagship: '',
        deluxecheck: false,
        flagshipchecks: false,
        checks:false
      })
    } else if (e.detail.value == '1'){
      this.setData({
        stander: 'stander', 
        deluxe: '',
        flagship: '',
        deluxecheck: false,
        flagshipchecks: false,
        checks: true
      })
    } else if (e.detail.value == '2') {
      this.setData({
        stander: '',
        deluxe: 'deluxe',
        flagship: '',
        flagshipchecks: false,
        checks: false,
        deluxecheck:true,
      })
    } else if (e.detail.value == '3') {
      this.setData({
        stander: '',
        deluxe: '',
        flagship: 'flagship',
        checks: false,
        deluxecheck: false,
        flagshipchecks:true
      })
    }
    that.setData({
      visions: e.target.dataset.index
    })
    console.log(e.target.dataset.index)
    console.log(e.detail.value)
  },
  addEnterpriseVip:function(){
    let that = this
    console.log(that.data.enterpriseId)
    wx.request({
      url: common.formal + '/addEnterpriseVip.form',
      data: {
        enterpriseId: that.data.enterpriseId,
        identityStatus: that.data.visions == undefined ? '1':that.data.visions,
        enterType:'2',
        objectId: that.data.objectId
      },
      method: 'get',
      success: function (json) {
        wx.showToast({
          title: "已发送请求",
          duration: 1000,
          mask: false,
        })
        setTimeout(function(){
            wx.reLaunch({
              url: './../Myinfor/Myinfor',
            })
        },1300)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    console.log(options)
    this.setData({
      objectId: options.objectId,
      enterpriseId: options.enterpriseId
    })
    console.log(that.data.objectId)
    this.checkvision(options.enterpriseId)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})